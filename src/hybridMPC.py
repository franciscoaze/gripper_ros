#!/usr/bin/env python
# For some unknown reason, it is necessary to import the pyexpat and matlab.engine modules before anything else.
import pyexpat
import matlab.engine as meng
import rospy
import cplex
import numpy as np
import pickle
import sys
from pytests.msg import Hstates
from pytests.msg import DroneStates
from pytests.msg import ArduinoInfo

class hybridModel():
    """
    hybridModel class
    Contains all the information regarding the different parameters of the hybrid model.
    """
    def __init__(self):
        """
        Gets the hybrid model information either from a raw hysdel file using the Matlab engine ('m')
        or from a pickle file ('p')
        """
        if (len(sys.argv) > 1):
            opt = str(sys.argv[1])
        else:
            opt = 'h'

        if(opt == 'p'):
            # Gets data from pickle file
            print('[HMPC]: Getting data from saved file...')
            file = open(r'hysdata.pkl', 'rb')
            data= pickle.load(file)
            file.close()
            print('[HMPC]: Done.')

        else:
            # Gets data from hysdel file
            # Start matlab engine
            print('[HMPC]: Starting matlab engine...')
            eng = meng.start_matlab()
            # It is necessary to specify the engine's directory
            eng.cd(r'/home/dsor-replace2/catkin_ws/src/pytests/src', nargout=0)
            # Run function mat2py providing all the necessary information for the problem definition
            print('[HMPC]: Getting hybrid model data...')
            data = eng.mat2py()
            # Quit matlab engine
            eng.quit()
            # Save data structures to pickle file for future use.
            print('[HMPC]: Saving...')
            f = open(r'hysdata.pkl', 'wb')
            pickle.dump(data, f)
            f.close()
            print('[HMPC]: Done.')

        # Retrieve the necessary data from the data file.
        # VERY IMPORTANT to cast all the Matlab data into a np array to increase computational efficiency (10x faster)
        self.H = np.array(data['MPC']['H'])
        self.horizon = data['MPC']['horizon']
        self.A = np.array(data['MPC']['A'])
        self.b0 = np.array(data['MPC']['b'])
        self.D = np.array(data['MPC']['D'])
        self.Cx = np.array(data['MPC']['Cx'])
        self.ivar = np.array(data['MPC']['ivar'], dtype='int32')[0]
        self.uvar = np.array(data['MPC']['uvar'], dtype='int32')[0]
        self.dvar = np.array(data['MPC']['dvar'], dtype='int32')[0]
        self.zvar = np.array(data['MPC']['zvar'], dtype='int32')[0]
        self.nu = np.array(data['mld']['nu'], dtype='int32')
        self.nd = np.array(data['mld']['nd'], dtype='int32')
        self.nz = np.array(data['mld']['nz'], dtype='int32')

        self.A1 = np.array(data['mld']['A1'])
        self.B1 = np.array(data['mld']['B1'])
        self.B2 = np.array(data['mld']['B2'])
        self.B3 = np.array(data['mld']['B3'])

        # Determine the states variables.
        self.state_names = ['theta','theta_dot','x','x_dot','y','y_dot','z','z_dot','yaw','yaw_dot','ph','fclosed']
        self.states = np.zeros((len(self.state_names),1))
        self.states[10] = 1

        # Determine the input variables.
        self.input_names = ['u','ux','uy','uz','uyaw']
        self.inputs = np.zeros((len(self.input_names),1))

        # Determine the auxiliary (or weighted) variables.
        self.y_names = ['pen','closed_out','zone','bg','inter']
        self.y = np.zeros((len(self.y_names),1))

        # Determine the references map.
        self.refx = np.array(data['ref'], ndmin=2)
        self.ryt = np.zeros((len(self.y_names), 1))
        self.rut = np.array([0,1,1,0,0], ndmin=2).transpose()

        self.next_phase = 1
        self.theta = 0

    def update_Hstates(self,new_states):
        """
        Update the states array to include the newly computed states from the optimization problem.
        :param new_states: states to be added to the states array
        """
        # States are vertical vector stacked horizontally.
        self.states = np.hstack([self.states,new_states])
        self.next_phase = int(round(new_states[10][0]))

    def get_last(self):
        return self.states[:,[-1]]

class timingOp():
    """
    timingOP class
    Auxiliary class to help monitoring the timing of desired operations.
    """
    def __init__(self,title=''):
        self.t = []
        self.names = []
        self.title = title
        self.t_start = rospy.get_rostime()

    def add(self,name):
        self.t.append(rospy.get_rostime() - self.t_start)
        self.names.append(name)
        self.t_start = rospy.get_rostime()

    def showT(self):
        ts = ['{0:2.5f}'.format(x.nsecs * (10 ** (-9))) for x in self.t]
        print('{}'.format(zip(self.names, ts)))

    def showSum(self):
        total = sum([x.nsecs * (10 ** (-9)) for x in self.t])
        print(self.title,total)
        return total

    def fromLast(self):
        final = (rospy.get_rostime() - self.t_start).nsecs * (10 ** (-9))
        print(('t0',final))

    def reset(self):
        self.t=[]
        self.names = []
        self.t_start = rospy.get_rostime()

class optimProb():
    """
    optimProb class
    Contains the main structures to run an optimization problem, adapted to Model Predictive Control.
    """
    def __init__(self, mode ='sim'):
        # Instantiate Cplex class
        self.P = cplex.Cplex()
        self.lastSol = []
        self.lastU = []

        # Instantiate Hybrid Model class
        self.HM = hybridModel()

        # Publisher sending the computed hybrid states to be used as references in the drone controller
        self.pub = rospy.Publisher('Hstates', Hstates, queue_size=10)
        self.send_msg = Hstates()
        self.send_msg.status = 1

        # Intialize auxiliary variables
        self.theta = 0
        self.mode = mode

        self.setproblemdata('gripper')

    def get_arduino_info(self,data):
        """
        Gets the real value for the gripper angle (theta). This method is called whenever a new data packet is published
        to the ArduinoInfo topic.
        :param data: data from arduino topic
        """
        pi = 3.14159265359
        if data.status ==1:
            self.mode = 'sim'
        elif data.status ==2:
            self.mode = 'real'
        self.theta = -data.gripper_angle*pi/180
        # print("theta: {}".format(self.theta))

    def setproblemdata(self,namep='',x0 = None):
        """
        Function to define initial problem conditions and variables that will stay
        unchanged throughout the optimization problem.
        :param namep: optimization problem name
        :param x0: initial conditions (not used)
        """
        self.P.set_problem_name(namep)

        # Set verbose levels to a minimum
        self.P.set_log_stream(None)
        # self.P.set_error_stream(None)
        self.P.set_warning_stream(None)

        # Set some parameters
        ps = self.P.create_parameter_set()
        self.P.parameters.tune.dettimelimit.set(0.003)
        self.P.parameters.timelimit.set(0.1)

        # Define if it is a maximization or minimization problem:
        # cost function is to be minimized
        self.P.objective.set_sense(self.P.objective.sense.minimize)

        # Add vector for linear objective function (f).
        # This value will be continuously updated in each iteration, with:
        # theta = [states;rx;ry;ru;rz]' (row vector)
        # D = matrix containing parameter linear costs
        # f = theta * D'
        states = self.HM.get_last()
        rxt = self.HM.refx[0].reshape((self.HM.refx[0].size,1))
        ryt = self.HM.ryt
        rut = self.HM.rut
        theta = np.vstack([states,rxt,ryt,rut]).transpose()
        f_obj = np.matmul(theta,self.HM.D.transpose()).astype('float')
        self.P.variables.add(obj=f_obj.tolist()[0])

        # Add matrix for quadratic objective function (H):
        (Hr,Hc) = np.nonzero(self.HM.H)
        H_vals = self.HM.H[(Hr,Hc)]
        # Cplex API requires the arrays to be defined as the rows and columns of non zero entries and their respective
        # values. Example: ((0,0,10),(2,1,20))
        self.P.objective.set_quadratic_coefficients(zip(Hr,Hc,H_vals))

        # Add vector for linear constraints (bineq & beq):
        # Define type of constraint - GLER - greater than, less than, equal, ranged
        # Current problem only has inequalities ("L"). This value will be continuously updated in each iteration.
        # b = b0 + Cx*states
        b = self.HM.b0 + np.matmul(self.HM.Cx,states)
        senses = "L"*len(b)
        self.P.linear_constraints.add(rhs = b.transpose().tolist()[0], senses=senses)

        # Define linear constraints matrix (Aineq & Aeq):
        (Ar,Ac) = np.nonzero(self.HM.A)
        A_vals = self.HM.A[(Ar,Ac)]
        self.P.linear_constraints.set_coefficients(zip(Ar, Ac, A_vals))

        # Define variable types using - CIBSN - continuous, integer, binary, semi-continuous, semi-integer:
        n_var = self.HM.A.shape[1]
        var_list = list("C"*n_var)
        for i in self.HM.ivar:
            var_list[i-1]="B"
        self.P.variables.set_types(zip(range(n_var),var_list))
        # Define variable's upper and lower bounds:
        # ... for u variables:
        lb_u = [-100 for x in range(len(self.HM.uvar))]
        lb_zip_u = zip([x-1 for x in self.HM.uvar.tolist()],lb_u)
        self.P.variables.set_lower_bounds(lb_zip_u)
        # ... for z variables:
        lb_z = [-1 for x in range(len(self.HM.zvar))]
        lb_zip_z = zip([x-1 for x in self.HM.zvar.tolist()],lb_z)
        self.P.variables.set_lower_bounds(lb_zip_z)
        # print(self.P.variables.get_lower_bounds())
        self.P.set_results_stream(None)
        self.P.parameters.tune_problem()


    def updateoptim(self,states):
        """
        Update optimization problem for the next iteration, considering the newly computed states.

        :param states:
        """
        # Update vector for linear objective function (f):
        # theta = [states;rx;ry;ru;rz]' (row vector)
        # D = matrix containing parameter linear costs
        # f = theta * D'
        # states = self.HM.get_last()
        rxt = self.HM.refx[self.HM.next_phase-1].reshape((self.HM.refx[0].size, 1)) # Update references for next phase
        ryt = self.HM.ryt
        rut = self.HM.rut
        theta = np.vstack([states, rxt, ryt, rut]).transpose()
        f_obj = np.matmul(theta, self.HM.D.transpose()).astype('float')
        self.P.objective.set_linear(zip(range(f_obj.shape[1]),f_obj[0]))

        # Update vector for linear constraints (bineq & beq):
        # b = b0 + Cx*states
        b = self.HM.b0 + np.matmul(self.HM.Cx, states)
        self.P.linear_constraints.set_rhs(zip(range(b.shape[0]),b.transpose()[0]))

        self.P.parameters.tune_problem()

    def solveiter(self):
        """
        Use this function to solve the current optimization problem after all the correspondent
        iteration variables have been updated.

        :return x: vector of predicted states for the next iteration
                u: vector of inputs for the drone and gripper
        """
        # Solve the optimization problem
        self.P.solve()
        sol = self.P.solution

        # Get the different variables from the optimization solution vector
        if(sol.get_status() in {103,108}):
            # Some error in the solving process
            x = self.lastSol
            u = self.lastU
            self.send_msg.status = -1
        else:
            xmin = np.array(sol.get_values())
            u = xmin[self.HM.uvar[0:prob.HM.nu]-1].reshape(prob.HM.nu,1)
            d = xmin[self.HM.dvar[0:prob.HM.nd]-1].reshape(prob.HM.nd,1)
            z = xmin[self.HM.zvar[0:prob.HM.nz]-1].reshape(prob.HM.nz,1)
            x = np.matmul(self.HM.A1,self.HM.get_last())+np.matmul(self.HM.B1,u) + np.matmul(self.HM.B2,d) + np.matmul(self.HM.B3,z)
            self.lastSol = x
            self.lastU = u
            self.send_msg.status = 1

        return x,u

    def runMPC(self,data):
        """
        Get states from the main drone loop and solve current optimization iteration. This method is called whenever a
        new data packet is published to the DroneStates topic.
        :param data: drone states
        """
        # Read data from drone dynamics
        states = self.HM.get_last()
        states[[2,4,6]] = np.array(data.p).reshape(3,1)
        states[[3,5,7]] = np.array(data.v).reshape(3,1)

        # Use the real gripper angle or the calculated one.
        if self.mode == 'real':
            states[0] = self.theta

        # Update and solve the new optimization problem
        self.updateoptim(states)
        x, u = self.solveiter()
        self.HM.update_Hstates(x)

        # Publish hybrid states message to topic Hstates
        self.send_msg.p = x[[2,4,6]].transpose().tolist()[0]
        self.send_msg.v = x[[3,5,7]].transpose().tolist()[0]
        self.send_msg.u = u.transpose().tolist()[0][0]
        self.send_msg.phase = self.HM.next_phase
        self.send_msg.theta = x[0][0]
        self.send_msg.header.stamp = rospy.Time.now()
        self.pub.publish(self.send_msg)

if __name__ == '__main__':
    try:
        # Initialize ROS nodes
        rospy.init_node('HMPC', anonymous=True)
        # Instantiate optimProb class
        prob = optimProb(mode = 'sim')

        # Set Subscriber to listen to the drone states and arduino data
        rospy.Subscriber("drone_states", DroneStates, prob.runMPC)
        rospy.Subscriber("arduino_info", ArduinoInfo, prob.get_arduino_info)

        # Send something to the HStates topic to notify that this node is set to go
        print('[HMPC]: Problem set. Starting:')
        send_msg = Hstates()
        send_msg.status = 0
        for i in range(10):
            prob.pub.publish(send_msg)
            rospy.sleep(0.01)

        rospy.spin()
    except rospy.ROSInterruptException:
        pass