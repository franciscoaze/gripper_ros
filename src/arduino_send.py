#!/usr/bin/env python

import rospy
from std_msgs.msg import Int32
from pytests.msg import Hstates
from pytests.msg import ArduinoInfo
import serial
import sys

class arduino_send:
    """
    arduino_send class
    Communicates with the Arduino via Serial port, sending it the computed gripper motor input values.
    """
    def __init__(self):
        # Initialize ROS node and topics
        rospy.init_node('serial_talker',anonymous=True)
        rospy.Subscriber("arduino_val",Int32,self.callbackInt)
        rospy.Subscriber("Hstates", Hstates, self.callbackHstate)
        self.pub = rospy.Publisher('arduino_info', ArduinoInfo, queue_size=10)
        rospy.sleep(0.1)

        self.send_msg = ArduinoInfo()
        self.last_tm = 0

        self.start = "O20" # String message to send to the arduino in order to set the initial position. "O" followed
        # by the angle in degrees.
        # This initial angle can also be passed as a function argument.
        if (len(sys.argv) > 1):
            self.start = str(sys.argv[1])
        print('[Arduino]: Init message - {}'.format(self.start))

        # Try different ports to initialize the serial connection.
        ports = ['/dev/ttyUSB0', '/dev/ttyUSB1']
        while len(ports)>0:
            p = ports.pop()
            try:
                self.ser = serial.Serial(port = p, baudrate = 115200,timeout = 0,dsrdtr = True)
                succ = True
            except:
                succ = False

        if succ==False:
            print('[Arduino]: Ports not available. Closing node...')
            rospy.signal_shutdown('Arduino not connected')
            sys.exit()
        
        rospy.sleep(1)
        self.intialize_arduino()
    
    def intialize_arduino(self):
        """
        Communicate with the Arduino board in order to set it to its desired initial conditions.
        """
        if self.start is "K":
            # If the starting string is "K", then the Arduino will bypass the homing operation and go straight to normal
            # functioning.
            message = "K10\n"
            self.send_msg.status = 2
            self.pub.publish(self.send_msg)
        elif self.start.startswith('O'):
            # If the starting string is "O", then the Arduino will move the gripper arm to the desired intial poition.
            m = self.start.split('O')
            try:
                ang = int(m[1])
                if (ang>=5 and ang<=85):
                    message = "O{}\n".format(ang)
                else:
                    message = "020"
            except:
                message = "O{}\n".format(20)
        else:
            message = "O{}\n".format(20)
            
        for x in range(20):
            self.ser.write(message)
        
    def callbackInt(self, data):
        """
        Listens to the arduino_val topic and sends that value as a motor input to the Arduino.
        """
        val = data.data
        message = "A{}\n".format(val)
        # print("Sent: " + message)
#         rospy.loginfo(message)
        
        self.ser.write(message)

    def callbackHstate(self, data):
        """
        Listens to the H_states topic, converts the computed value and sends it as a motor input to the Arduino.
        """
        val = -100 * data.u  + 1000
        message = "A{0:.0f}\n".format(val)
        # print("Sent: " + message)
        #         rospy.loginfo(message)

        self.ser.write(message)

if __name__ == '__main__':
    try:
        d = arduino_send()
        rospy.spin()
    except rospy.ROSInterruptException: 
        pass