#! /usr/bin/env python
# -*- coding: utf-8 -*-
import curses 
import math
import numpy as np

import rospy
import rosbag
from std_msgs.msg import Int32
from pytests.msg import ArduinoInfo

class TextWindow():

    _screen = None
    _window = None
    _num_lines = None

    def __init__(self, stdscr, lines=10):
        self._screen = stdscr
        self._screen.nodelay(True)
        curses.curs_set(0)

        self._num_lines = lines

    def read_key(self):
        keycode = self._screen.getch()
        return keycode if keycode != -1 else None

    def clear(self):
        self._screen.clear()

    def write_line(self, lineno, message):
        if lineno < 0 or lineno >= self._num_lines:
            raise ValueError, 'lineno out of bounds'
        height, width = self._screen.getmaxyx()
        y = (height / self._num_lines) * lineno
        x = 10
        for text in message.split('\n'):
            text = text.ljust(width)
            self._screen.addstr(y, x, text)
            y += 1

    def refresh(self):
        self._screen.refresh()

    def beep(self):
        curses.flash()

class KeyTeleop():

    _interface = None

    _linear = None
    _angular = None

    def __init__(self, interface):
        self._interface = interface
        self._pub_cmd = rospy.Publisher('arduino_val', Int32)

        self._hz = rospy.get_param('~hz', 20)

        self._max_vel = 1
        
        self.gripper_angle = 'None'
        self.current = 0
        self.current_threshold = 0

        rospy.Subscriber("arduino_info",ArduinoInfo,self.listenArduino)

        self.bag = rosbag.Bag('ardu.bag', 'w')

    def run(self):
        self._linear = 0
        self._running = True
        rate = rospy.Rate(self._hz)
        while self._running is True:
            keycode = self._interface.read_key()
            if keycode:
                if self._key_pressed(keycode):
                    self._publish()
            else:
                self._publish()
                rate.sleep()

    def _get_twist(self, linear, angular):
        twist = Twist()
        if linear >= 0:
            twist.linear.x = self._forward(1.0, linear)
        else:
            twist.linear.x = self._backward(-1.0, -linear)
        twist.angular.z = self._rotation(math.copysign(1, angular), abs(angular))
        return twist

    def _key_pressed(self, keycode):
        # Left - Close
        # Right - Open
        movement_bindings = {
            curses.KEY_LEFT:  -0.1,
            curses.KEY_RIGHT: 0.1,
            ord('.'): 0.01,
            ord(','): -0.01,
        }
        fast_bindings = {
            curses.KEY_UP: 1,
            curses.KEY_DOWN: -1
        }
        stop_bindings = {
            ord(' '): (0, 0),
        }
        if keycode == ord('t'):
            self.enableTest()
            return True
            
        if keycode == ord('q'):
            # Stop running
            self.bag.close()
            try:
                self.bag.flush()
            except:
                self._running = False
            
        if keycode in movement_bindings:
            acc = movement_bindings[keycode]
            ok = False
            linear = self._linear + acc
            if abs(linear) <= self._max_vel:
                self._linear = linear
                ok = True
            if not ok:
                self._interface.beep()
        elif keycode in fast_bindings:
            acc = fast_bindings[keycode]
            self._linear = acc
        elif keycode in stop_bindings:
            acc = stop_bindings[keycode]
            self._linear = 0
#
#        elif keycode == ord('q'):
#            rospy.signal_shutdown('Bye')
        else:
            return False

        return True
    
    def listenArduino(self,data):

        self.gripper_angle = data.gripper_angle
        gripper_angle = Int32()
        gripper_angle.data = data.gripper_angle
        self.bag.write('gripper_angle', gripper_angle)

        self.current = data.current
        current = Int32()
        current.data = self.current
        self.bag.write('current', current)

        current_stall = Int32()
        current_stall.data = data.current_stall
        self.bag.write('current_stall', current_stall)

        self.current_threshold = data.current_threshold

        u = Int32()
        u.data = self._linear*100
        self.bag.write('u',u)
        
    def enableTest(self):
        self._interface.clear()
        self._interface.write_line(2, 'TESTING GRIPPER CAPABILITIES')
        self._interface.refresh()
        rate = rospy.Rate(20)
        while (self.gripper_angle>8):
            speed = 950
            self._pub_cmd.publish(speed)
            rate.sleep()
        self._pub_cmd.publish(1000)
        rospy.sleep(1)
        self._interface.write_line(4, 'Acquiring max closing speed...')
        self._interface.refresh()
        pos_arr = []
        time_arr = []
        speed = 1100
        self._pub_cmd.publish(speed)
        while ((self.current_threshold>self.current) or (self.gripper_angle<30)):
            speed = 1100
            self._pub_cmd.publish(speed)
            pos_arr.append(self.gripper_angle)
            time_arr.append(rospy.get_time())
            rate.sleep()
        self._pub_cmd.publish(1000)
        
        vel=[0]
        for i in range(len(time_arr)-1):
            dx = pos_arr[i+1]-pos_arr[i]
            dt = time_arr[i+1]-time_arr[i]
            vel.append(dx/dt)
        vel = np.array(vel)
        vel_ok = vel[vel!=0.0]
        arr = np.column_stack((np.column_stack((time_arr,pos_arr)),vel))
        np.savetxt('/home/dsor-replace2/Desktop/GripperTests_log.txt',arr,fmt='%-6.3f')
        self._interface.write_line(6, 'Acquired {0} points. Mean (median) velocity is {1:3.3f} ({2:3.3f).'.format(len(pos_arr),vel_ok.mean(),np.median(vel_ok)))
        self._interface.write_line(7, 'Maximum velocity is {}.'.format(vel.max()))
        self._interface.refresh()
        rospy.sleep(1)
        self._pub_cmd.publish(1000)
        self._linear = 0
        while not self._interface.read_key(): 
            pass

    def _publish(self):
        self._interface.clear()
        self._interface.write_line(2, 'Gripper velocity: {0:.0%}'.format(self._linear))
        self._interface.write_line(3, 'Gripper angle: {}'.format(str(self.gripper_angle)))
        self._interface.write_line(5, 'Use left/right keys to close/open the gripper with 10% speed increments,')
        self._interface.write_line(6, 'down/up keys to close/open the gripper at full speed,')
        self._interface.write_line(7, 'space to stop.')
        self._interface.write_line(8, 'Press t to enter test mode, q to exit program.')
        self._interface.refresh()

        send_msg = 100*self._linear + 1000
        self._pub_cmd.publish(send_msg)
        
def main(stdscr):
    rospy.init_node('key_telegripper')
    
    app = KeyTeleop(TextWindow(stdscr))
    app.run()

if __name__ == '__main__':
    try:
        curses.wrapper(main)
    except rospy.ROSInterruptException:
        pass