#!/usr/bin/env python
from matplotlib import pyplot as plt
import numpy as np
import rospy
import math
import scipy.linalg as sp
from pytests.msg import DroneStates
from pytests.msg import Hstates
from pytests.msg import ArduinoInfo
from pytests.msg import PPose
from hybridMPC import timingOp
import sys

def expm2(A):
    """
    Compute the matrix exponential using eigenvalue decomposition.
    Parameters
    ----------
    A : (N, N) array_like
        Matrix to be exponentiated
    Returns
    -------
    expm2 : (N, N) ndarray
        Matrix exponential of `A`
    """
    A = np.asarray(A)
    t = A.dtype.char

    s,vr = sp.eig(A)
    vri = sp.inv(vr)
    r = np.dot(np.dot(vr,np.diag(np.exp(s))),vri)

    return r.real.astype(t)


def sat(val,minval,maxval):
    """
    Function that 'squashes' a value (or array) into predefined limits.
    :param val: value or array to be squashed
    :param minval: lower bound
    :param maxval: higher bound
    :return:
    """
    s = val.shape
    new=[]
    for v in val:
        new.append(min(maxval,max(v[0],minval)))
    return np.array(new,ndmin=2).reshape(s)

class checkArduino:
    """
    checkArduino class

    Serves as an auxiliary mechanism to check if the arduino connection is estabilished.
    """
    def __init__(self):
        self.sub = rospy.Subscriber("arduino_info", ArduinoInfo, self.callback)
        self.go = 0
        self.connected = False
        self.stats_str = {1: 'Arduino online.',
                          2: 'Arduino homed.',
                          -1: 'No usb connected.'}
    def callback(self,data):
        """
        Monitorizes incoming message status from the arduino nodes.
        :param data: status from arduino
        """
        status = data.status
        if (status==2 and self.connected==True):
            self.go = 1
            self.sub.unregister()
            print("*****{}*****".format(self.stats_str[status]))
        elif(status==1 and self.connected==False):
            self.connected = True
            print("[Arduino]: {}".format(self.stats_str[status]))
        elif (status == -1 and self.connected == False):
            print("[Arduino]: {}".format(self.stats_str[status]))

# DRONE DYNAMICS
class states:
    """
    states class

    Contains all the information regarding the drone states and the necessary functions to handle them.
    """
    def __init__(self):
        self.p = np.array([[0],[0],[0]],ndmin=2)
        self.v = np.array([[0],[0],[0]],ndmin=2)
        self.wB = np.array([[0],[0],[0]],ndmin=2)
        self.t = np.array([0],dtype='float64')
        self.pub = rospy.Publisher('drone_states', DroneStates, queue_size=10)
        self.send_msg = DroneStates()

        self.phase = 0

        self.p0x = 1 # initial position of aruco camera in relation to the parcel
        self.camGo = 0
    
    def update_states(self,p,v,wB,t):
        """
        Updates state arrays with current state values.
        :param p:
        :param v:
        :param wB:
        :param t:
        """
        # Experimentar com b[:,:-1] = a
        self.p = np.hstack([self.p,p])
        self.v = np.hstack([self.v,v])
        self.wB = np.hstack([self.wB,wB])
        self.t = np.hstack([self.t,t])

    def update_states_aruco(self,data):
        """
        Gets object pose from ArUco node and uses it to calculate the drone position.
        :param data:
        """
        px = data.PposC[1]
        py = data.PposC[0]
        pz = data.PposC[2]
        l0x = 0.16

        # Adapt it to the current problem
        if (self.p[0,-1]>3 and self.phase<=2):
            self.p[0,-1] = 3 + self.p0x + l0x - px

        # Check status messages to initialize module
        if data.status == 1 and self.camGo == 0:
            self.camGo = 1
            print('[ArUco]: Camera is online.')
            print('*****Camera Connected.*****')

    def get_last(self):
        """
        Get last state values.
        """
        return self.p[:,[-1]], self.v[:,[-1]], self.wB[:,[-1]], self.t[-1]
    
    def print_states(self):
        """
        Prints current state variables to available output screen.
        """
        print('Time: {0:6.2f}'.format(self.t[-1]))
        print('Position: x:{0:7.3f}, y:{1:7.3f}, z:{2:7.3f}'.format(*self.p[:,-1]))
        print('Velocity: vx:{0:6.3f}, vy:{1:6.3f}, vz:{2:6.3f}'.format(*self.v[:,-1]))
        print('Ang.Vel.: wx:{0:6.3f}, wy:{1:6.3f}, wz:{2:6.3f}'.format(*self.wB[: ,-1]))
        
    def pub_states(self):
        """
        Publish current states to predefined topic
        """
        self.send_msg.p = self.p[:,-1].tolist()
        self.send_msg.v = self.v[:,-1].tolist()
        self.send_msg.wB = self.wB[:,-1].tolist()
        self.send_msg.t = [self.t[-1]]
        self.send_msg.header.stamp = rospy.Time.now()
        self.pub.publish(self.send_msg)

    def plot_states(self):
        """
        Plot states.

        [unused]
        """
        p = self.p[:,-1].tolist()
        plt.plot(self.t[-1],p[0], '*')
        plt.axis("equal")
        plt.draw()
        plt.pause(0.00000000001)

           
class refs:
    """
    refs class

    Contains all the information regarding the drone references and the necessary functions to handle them.
    """
    def __init__(self):
        # State references
        self.p = np.array([[0],[0],[0]],ndmin=2)
        self.v = np.array([[0],[0],[0]],ndmin=2)
        self.a = np.array([[0],[0],[0]],ndmin=2)
        self.jerk = np.array([[0],[0],[0]],ndmin=2)
        self.yaw = np.array([0],ndmin=2)
        self.yaw_dot = np.array([0],ndmin=2)
        self.pitch = np.array([0],ndmin=2)
        self.roll = np.array([0],ndmin=2)

        self.phase = 0
        # Auxiliary variables
        self.K = 2 # Check if model gets new references or not.
        self.wait = False
        self.go = 0
        
    def update_refs(self,data):
        """
        Updates references with values calculated from the MPC
        :param data: position, velocity and phase values from hybridMPC
        """
        # Experimentar com b[:,:-1] = a
        if (data.status==1 or self.go==1):
            self.p =  np.hstack([self.p,np.array(data.p).reshape(3,1)])
            self.v =  np.hstack([self.v,np.array(data.v).reshape(3,1)])
            self.wait = False
            self.phase = data.phase

    #        self.a = np.vstack([self.a,a])
    #        self.jerk = np.vstack([self.jerk,jerk])
    #        self.yaw = np.vstack([self.yaw,yaw])
    #        self.yaw_dot = np.vstack([self.yaw_dot,yaw_dot])
    #        self.pitch = np.vstack([self.pitch,pitch])
    #        self.roll = np.vstack([self.roll,roll])
        else:
            # Initialize module with status message.
            print('*****HMPC Connected.*****')
            self.go = 1


    def get_last(self):
        """
        Get last reference values.
        """
        return self.p[:,[-1]], self.v[:,[-1]],self.a[:,[-1]],self.yaw[-1],self.jerk[:,[-1]],self.yaw_dot[-1]  


class params:
    """
    params class
    Contains all the necessary drone, parcel and simulation parameters.
    """
    def __init__(self,H=45,W=100,L=225,D=0,mass =1.9):
        self.mass = mass
        self.g=9.81
        Ixx = mass*((W**2)/2 + (H**2)/2 + D)
        Iyy = mass*((L**2)/2 + (H**2)/2 + D)
        Izz = mass*((L**2)/2 + (W**2)/2)
        self.I = np.array([[Ixx,0,0],[0,Iyy,0],[0,0,Izz]])
        self.I=(self.I/self.g)/(1000**2) # In N m s^2

        self.Ts = 0.01 # Main loop time step
        self.Ts_att = 0.002
        self.T_MPC = 0.1

class droneDynamics():
    """
    droneDynamics class

    Simulates the dynamics of the drone and integrates other external modules such as: the hybrid MPC, the ArUco camera and the
    Arduino controlled gripper.
    """
    def __init__(self):
        # Instantiate the different classes
        self.states = states()
        self.refs = refs()
        self.par = params()

        self.Zw = np.array([0, 0, 1], ndmin=2).transpose()
        self.B2WR_k = np.array([[1, 0, 0], [0, 1, 0], [0, 0, 1]])

        rospy.Subscriber("Hstates", Hstates, self.refs.update_refs)
        rospy.Subscriber("aruco_pose", PPose, self.states.update_states_aruco)

        # Define the different working rates.
        self.rt_at = 1./self.par.Ts_att # Hz
        self.Ts_at = self.par.Ts_att
        self.rt = 1/self.par.Ts
        self.rate = rospy.Rate(self.rt) # 10hz , go through the loop 10 times per second

        # Set the controller gains.
        self.KR = 18
        self.Kw = 1
        self.Kp = 15
        self.Kv = 10

    def mainLoop(self):
        """
        Main loop for the drone dynamics. Each iteration of this loop corresponds to one time sample Ts.
        Inside this loop there is another faster loop running at rt_at dealing with the drone attitude.

        The drone dynamics and controller are adapted from the work done by
        Mellinger et. all in "Trajectory Generation and Control for Quadrotors".
        """

        # Get the current states variables and references.
        p_k, v_k, wB_k, t_k = self.states.get_last()
        p_ref, v_ref, a_ref, yaw_ref, jerk_ref, yaw_dot_ref = self.refs.get_last()
        self.states.phase = self.refs.phase

#       **** Position Controller ****
        # The position feedback loop is being done inside the MPC.
        # e_p = p_k - p_ref
        e_p = 0

        e_v = v_k - v_ref

        Fdes = -self.Kp*e_p - self.Kv*e_v + self.par.mass*self.par.g*self.Zw + self.par.mass*a_ref
        Zb = np.matmul(self.B2WR_k,self.Zw)

        u1 = np.dot(Fdes.transpose(),Zb)
        u1 = sat(u1,0,40)

        k_att = 0
        while(k_att<=(self.rt_at/self.rt)):
            # * Attitude Controller
            # 14/11 12:02 - loop a demorar 0.007 ~ 0.012
            # 14/11 15:22 - loop a demorar 0.0058 ~ 0.0088
            Zb_des = Fdes / np.linalg.norm(Fdes)
            Xa_des = np.array([math.cos(yaw_ref),math.sin(yaw_ref),0],ndmin=2).transpose()

            Yb_des = np.cross(Zb_des.transpose(),Xa_des.transpose()) / np.linalg.norm(np.cross(Zb_des,Xa_des,axisa=0,axisb =0)).astype('float32')
            Xb_des = np.cross(Yb_des,Zb_des.transpose())

            B2WR_des = np.hstack([Xb_des.transpose(),Yb_des.transpose(),Zb_des]).astype('float32')
            v_map = np.matmul(B2WR_des.transpose(),self.B2WR_k)*0.5 - np.matmul(self.B2WR_k.transpose(),B2WR_des)*0.5
            e_R = np.array([v_map[2][1],v_map[0][2],v_map[1][0]],ndmin=2).transpose()

            h_w = (self.par.mass / u1)*(jerk_ref - (np.dot(Zb_des.transpose(),jerk_ref)*Zb_des))
            p_des = np.dot(Yb_des,-h_w)
            q_des = np.dot(Xb_des,h_w)

            A = np.hstack([Zb_des , -Xb_des.transpose() , -Yb_des.transpose()])
            B = -Xb_des.transpose()*p_des - Yb_des.transpose()*q_des + np.array([0,0,1],ndmin=2).transpose()*yaw_dot_ref
            A = A.astype('float32')
            B = B.astype('float32')

            y = np.linalg.solve(A,B)

            wB_des = np.array([p_des[0,0],q_des[0,0],y[0]],ndmin=2).transpose()
            e_w = wB_k - wB_des

            ut = -self.KR*e_R- self.Kw*e_w
            ut = sat(ut,-2,2)
            u = np.vstack([u1[0],ut])

            # Angular Velocities
            w_hat = np.array([[0,-wB_k[2],wB_k[1]],[wB_k[2],0,-wB_k[0]],[-wB_k[1],wB_k[0],0]]).astype('float32')

            smtng = np.matmul(np.linalg.inv(self.par.I).astype('float32'),(np.matmul(-w_hat,np.matmul(self.par.I,wB_k)) + ut).astype('float32'))
            wB_kFplus = wB_k + (self.Ts_at)*smtng

            # Rotation Matrix
            B2WR_kFplus = np.matmul(self.B2WR_k,expm2(-w_hat*self.Ts_at).transpose())
            wB_k = wB_kFplus.astype('float32')
            self.B2WR_k = B2WR_kFplus.astype('float32')

            k_att = k_att+1

        # Linear Velocites
        v_kplus = v_k + self.par.Ts*(-self.par.g*self.Zw + (u[0]*Zb/self.par.mass))

        # Position
        p_kplus = p_k + self.par.Ts*v_k

        t_k = t_k + self.par.Ts
        self.states.update_states(p_kplus,v_kplus,wB_kFplus,t_k)

        # self.states.print_states()

    def waitHMPC(self):
        """
        Function that waits for a response from the hybrid MPC module
        """
        try:
            msg = rospy.wait_for_message("Hstates", Hstates,10) # TODO ver qual o melhor timeout
            # Sera que MPC esta a ser rapido demais?
            # print(msg)
            self.refs.update_refs(msg)
            # print('HMPC good.')
        except(rospy.exceptions.ROSException):
            print('Reading message timed out.')

if __name__ == '__main__':
    try:
        # Check if an extra argument is being passed:
        # 'r' - runs the loop with drone.rate.sleep().
        # 'i' - waits for the user to press a key after each iteration.
        # 'MPC' - Tries to connect to the MPC, Arduino, and Camera modules. Runs loop according to drone.rate.sleep().
        if (len(sys.argv) > 1):
            opt = str(sys.argv[1])
        else:
            opt = 'r'

        # Initialize node and necessary objects
        rospy.init_node('drone_dynamicsMPC', anonymous=True, disable_signals=True)
        ardu = checkArduino()
        drone = droneDynamics()

        t1 = rospy.get_time()
        r = rospy.Rate(10)
        if (opt == 'MPC'):
            # Wait for MPC to boot
            status_str = ['HMPC','ArUco','Arduino']
            while True:
                t = rospy.get_time() - t1
                sys_status = [drone.refs.go,drone.states.camGo,ardu.go]
                if (t>15):
                    if drone.refs.go == 1 :
                        # Start program without arduino
                        errors = [status_str[i] for i in range(len(status_str)) if not sys_status[i]]
                        print('--------Booting without {}--------'.format(" & ".join(errors)))
                        break
                    print('Modules did not start on time. Exiting program...')
                    rospy.signal_shutdown('Modules did not start on time.')
                    sys.exit()
                elif sys_status == [1,1,1]:
                    # If MPC node is sending, camera module is good, and the communication with the Arduino is online, keep going.
                    print('--------All systems ready.--------')
                    break
                r.sleep()

        # Confirm that the program can start.
        try:
            a = raw_input('press any key to start running')
            if a== 'q':
                print('Exiting...')
                rospy.signal_shutdown('User quit program.')
                sys.exit()
        except:
            pass

        # Main loop - runs without communicating with the MPC for T_MPC/Ts iterations and then publishes to
        # DroneStates topic and waits for a response.
        times = []
        t_iter = rospy.get_rostime()
        print('Starting...')
        while not rospy.is_shutdown():
            if (drone.refs.wait == False): # TODO fazer algo com isto
                t_iter = rospy.get_rostime()
                if (drone.refs.K < ((drone.par.T_MPC / drone.par.Ts))):
                    # Run loop without using the HMPC
                    drone.mainLoop()
                else:
                    # Got references from HMPC
                    drone.refs.K = 0
                    drone.mainLoop()
                    drone.states.pub_states()
                    drone.refs.wait = True

                drone.refs.K = drone.refs.K + 1

                # Options for running loops:
                if opt == 'i':
                    # loop waits for user input
                    try:
                        input()
                    except:
                        pass
                elif opt == 'MPC':
                    # loop runs according to rospy.rate
                    drone.rate.sleep()
                    it_time = (rospy.get_rostime() - t_iter).nsecs* (10 ** (-9))
                    times.append(it_time)
                    if drone.states.t[-1] > 15:
                        break
                else:
                    # loop runs with rate
                    drone.rate.sleep()
                    if drone.states.t[-1] > 15:
                        break
            else:
                pass
        print('Finished.')

        # Plots relevant information
        ts = drone.states.t.tolist()
        ts.pop()
        plt.figure(1)
        plt.plot(ts, times, '.')
        plt.ylim(0, 0.1)

        plt.figure(2)
        plt.plot(drone.states.t, drone.states.p[0])
        plt.draw()
        plt.pause(0.00000000001)
        plt.show()
    except rospy.ROSInterruptException:
        pass



