SYSTEM HMPC2 {
	INTERFACE{
		INPUT {
			REAL u [-1,1]; /*1 - gripper motor input normalized*/
			REAL ux [-1,1]; /*2 - drone x velocity normalized*/
			REAL uy [-1,1]; /*3 - drone y velocity normalized*/
			REAL uz [-1,1]; /*4 - drone z velocity normalized*/
			REAL uyaw [-1,1]; /*5 - drone yaw velocity normalized*/
		}
		STATE {
			REAL theta [-1.5,1.5];	/*1 - gripper arm angle*/
			REAL theta_dot [-5,5];	/*2 - gripper arm angular velocity */
			
			REAL x [-100,100];	/*3 - drone position in the x axis*/
			REAL x_dot [-5,5];	/*4 - drone velocity in the x axis*/

			REAL y [-100,100];	/*5 - drone position in the y axis*/
			REAL y_dot [-5,5];	/*6 - drone velocity in the y axis*/
			
			REAL z[-10,10]; /*7 - drone position in the z axis*/
			REAL z_dot[-5,5]; /*8 - drone velocity in the z axis*/
			
			REAL yaw [-10,10]; /*9 - drone yaw angle */
			REAL yaw_dot [-10,10];	/*10 - drone yaw angle velocity*/
			
			REAL ph [1,10];	/*11 - current phase*/
			
			BOOL fclosed; /*12 - gripper force sensor (here simulated by the angle)*/
		}
		PARAMETER {
			/* Global problem parameters*/
			REAL Ts, v, vz, kyaw;
			/* Gripper dynamics parameters*/
			REAL k,closed_theta,pre;
			/* Drone dynamics parameters*/
			REAL safe_zone_x,grab_pos_x,drop_pos_x;
			REAL safe_zone_y,grab_pos_y,drop_pos_y;
			REAL grab_pos_z,drop_pos_z;
			REAL safety_v;
			/* Others */ 
			REAL margin;
		}
		OUTPUT{
			REAL dist;
			REAL pen;
			REAL closed_out;
			REAL zone;
			REAL bg;
			BOOL inter;
		}
	}
	IMPLEMENTATION{
		AUX {
			/*Position auxiliary variables*/
			BOOL passed, passed_zone, passed_margin, on_drop;
			BOOL passed_x,passed_zone_x,passed_margin_xL,passed_margin_xH,passed_margin_x,on_drop_x;
			BOOL passed_y,passed_zone_y,passed_margin_yL,passed_margin_yH,passed_margin_y,on_drop_y;
			BOOL passed_zL,passed_zH,passed_margin_z,on_drop_z;
			REAL safe_vel_x;
			REAL safe_vel_y;
			/*Gripper auxiliary variables*/
			REAL block_gripper;
			BOOL closed;
			BOOL closed_margin;
			BOOL prepared;
			/*States auxiliary variables*/
			REAL dtheta,dvx,dvy,dvz,dyaw,phase;
			/*Outputs*/
			REAL closed_dist, zone_u;
			BOOL pen_aux;
			/*HYSDEL ineficiencies...*/	
			BOOL isPH1,isPH2_high,isPH2_low,isPH2,isPH3_high,isPH3_low,isPH3,isPH4,isPH4_low,isPH4_high;
			BOOL closed_aux,cl_dst;
			REAL dist_calc_x,dist_calc_y,closed_dist_x,closed_dist_y;
			REAL secured;
			BOOL secure_aux;
		}
		
		AUTOMATA{
			fclosed = closed_aux;
		}
		AD {
			passed_x = (grab_pos_x) <= x; 
			passed_margin_xL = (grab_pos_x - 0.01) <= x;
			passed_margin_xH = (grab_pos_x + 0.01) >= x;
			passed_zone_x = (safe_zone_x) <= x; 	
			
			passed_y = (grab_pos_y) <= y; 
			passed_margin_yL = (grab_pos_y - 0.01) <= y;
			passed_margin_yH = (grab_pos_y + 0.01) >= y;
			passed_zone_y = (safe_zone_y) <= y; 	
			
			/*passed_z = (grab_pos_z) >= z; */
			passed_zL = (grab_pos_z + 0.01) >= z; 
			passed_zH = (grab_pos_z - 0.01) <= z; 
			
			on_drop_x = drop_pos_x - 0.01 <= x;
			on_drop_y = drop_pos_y - 0.01 <= y;
			on_drop_z = drop_pos_z - 0.01 <= z;
			
			isPH1 = ph <=1.1;
			
			isPH2_low = ph >= 1.9;
			isPH2_high = ph <= 2.9;
			
			isPH3_low = ph >= 2.9;
			isPH3_high = ph <= 3.9;
			
			isPH4_low = ph >= 3.95;
			isPH4_high = ph <= 4.9;
			
			closed_aux = theta - 0.001 <= closed_theta;	
			closed_margin = theta - margin <= closed_theta;
			prepared = theta>=pre;
			
			cl_dst = closed_dist_x >= closed_dist_y;
			
			secure_aux = secured>=0.9;
		}
		DA {	
			
			dist_calc_x = {IF !passed THEN grab_pos_x - x ELSE 0};
			dist_calc_y = {IF !passed THEN grab_pos_y - y ELSE 0};
			closed_dist_x = {IF closed_aux THEN dist_calc_x ELSE 0};
			closed_dist_y = {IF closed_aux THEN dist_calc_y ELSE 0};
			
			closed_dist = {IF (cl_dst) THEN closed_dist_x ELSE closed_dist_y};
			
			zone_u = {IF !passed_zone THEN u ELSE 0};
			
			safe_vel_x = {IF passed_zone&isPH2 THEN x_dot ELSE 0};
			safe_vel_y = {IF passed_zone&isPH2 THEN y_dot ELSE 0};
			
			block_gripper = {IF isPH3|isPH1 THEN u ELSE 0};
			
			secured = {IF fclosed THEN 1 ELSE (REAL passed_margin)};
			
			phase = {IF ((passed_zone&isPH1) || (secure_aux&isPH2) || (on_drop&isPH3) || ((!closed)&on_drop&isPH4)) THEN (ph + 1) ELSE ph};
		}
		LOGIC {
			passed_margin_x = passed_margin_xL&passed_margin_xH;
			passed_margin_y = passed_margin_yL&passed_margin_yH;
			passed_margin_z = (passed_zL&passed_zH);
			passed = passed_x&passed_y;
			passed_margin = passed_margin_x&passed_margin_y&passed_margin_z;
			passed_zone = passed_zone_x&&passed_zone_y;			
			on_drop = on_drop_x&on_drop_y&on_drop_z;
			
			isPH2 = isPH2_low&isPH2_high;
			isPH3 = isPH3_low&isPH3_high;
			isPH4 = isPH4_low&isPH4_high;
			
			pen_aux = !closed&passed&(isPH2|isPH3);
			
			closed = closed_margin;	
		}
		LINEAR{
			/*Gripper*/
			dtheta = -k*u;
			/*Drone*/
			dvx = ux*v;
			dvy = uy*v;
			dvz = uz*vz;
			dyaw = kyaw*uyaw;
			/*Problem*/
			
		}

		CONTINUOUS {
			theta_dot = dtheta;
			theta = theta + Ts*theta_dot;

			x_dot = dvx;
			x = x + Ts*x_dot;
			
			y_dot = dvy;
			y = y + Ts*y_dot;
			
			z_dot = dvz;
			z = z + Ts*z_dot;
						
			yaw_dot= dyaw;
			yaw = yaw + Ts*yaw_dot;
			
			ph = phase;
		}
		
		OUTPUT {
			dist = dist_calc_x;
			pen =  (REAL pen_aux);
			closed_out = closed_dist;
			zone = zone_u;
			bg = block_gripper;
			inter = !closed_aux&(!prepared)&isPH2;
		}
		
		MUST {
			theta <= 0.5;
			theta >= -2;
			safe_vel_x <= safety_v;
			safe_vel_y <= safety_v;
			phase >= ph;
			/*isPH3 -> closed_aux;*/
			passed_margin -> (!closed_aux&(!prepared));
			/*passed -> secure_aux;*/
			/*block_gripper<=0;*/
		}
		
	}	
}