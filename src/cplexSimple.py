#!/usr/bin/env python
# For some unknown reason, it is necessary to import the pyexpat and matlab.engine modules before anything else.
import pyexpat
import matlab.engine as meng
# import rospy
import cplex
import numpy as np
# from pytests.msg import Hstates
# from pytests.msg import DroneStates

class hybridModel():
    def __init__(self):
        # Start matlab engine
        print('Starting matlab engine...')
        eng = meng.start_matlab()
        eng.cd(r'/home/dsor-replace2/catkin_ws/src/pytests/src', nargout=0)
        # Run function mat2py providing all the necessary information for the problem definition
        print('Getting hybrid model data...')
        data = eng.mat2py()
        # Quit matlab engine
        eng.quit()
        print('Done.')
        # Retrieve the necessary data
        self.H = np.array(data['MPC']['H'])
        self.horizon = data['MPC']['horizon']
        self.A = np.array(data['MPC']['A'])
        self.b0 = data['MPC']['b']
        self.D = np.array(data['MPC']['D'])
        self.Cx = data['MPC']['Cx']
        self.ivar = np.array(data['MPC']['ivar'], dtype='int32')[0]
        self.uvar = np.array(data['MPC']['uvar'], dtype='int32')[0]
        self.dvar = np.array(data['MPC']['dvar'], dtype='int32')[0]
        self.zvar = np.array(data['MPC']['zvar'], dtype='int32')[0]
        self.nu = np.array(data['mld']['nu'], dtype='int32')
        self.nd = np.array(data['mld']['nd'], dtype='int32')
        self.nz = np.array(data['mld']['nz'], dtype='int32')

        self.A1 = np.array(data['mld']['A1'])
        self.B1 = np.array(data['mld']['B1'])
        self.B2 = np.array(data['mld']['B2'])
        self.B3 = np.array(data['mld']['B3'])

        # Determine the states variables.
        self.state_names = ['theta','theta_dot','x','x_dot','y','y_dot','z','z_dot','yaw','yaw_dot','ph','fclosed']
        self.states = np.zeros((len(self.state_names),1))
        self.states[10] = 1

        # Determine the input variables.
        self.input_names = ['u','ux','uy','uz','uyaw']
        self.inputs = np.zeros((len(self.input_names),1))

        # Determine the auxiliary (or weighted) variables.
        self.y_names = ['pen','closed_out','zone','bg','inter']
        self.y = np.zeros((len(self.y_names),1))

        # Determine the references map.
        self.refx = np.array(data['ref'], ndmin=2)
        self.ryt = np.zeros((len(self.y_names), 1))
        self.rut = np.array([0,1,1,0,0], ndmin=2).transpose()

    def update_Hstates(self,new_states):
        self.states = np.hstack([self.states,new_states])

    def get_last(self):
        return self.states[:,[-1]]

class optimProb():
    def __init__(self):
        # Create Cplex class
        self.P = cplex.Cplex()
        self.HM = hybridModel()

    def setproblemdata(self,namep,x0 = None):
        # Function to define initial problem conditions and variables that will stay
        # unchanged throughout the optimization problem.

        self.P.set_problem_name(namep)

        # Set verbose levels to a minimum
        self.P.set_log_stream(None)
        # self.P.set_error_stream(None)
        self.P.set_warning_stream(None)
        self.P.set_results_stream(None)

        # Define if it is a maximization or minimization problem:
        # cost function is to be minimized
        self.P.objective.set_sense(self.P.objective.sense.minimize)

        # Add vector for linear objective function (f).
        # This value will be continuously updated in each iteration, with:
        # theta = [states;rx;ry;ru;rz]' (row vector)
        # D = matrix containing parameter linear costs
        # f = theta * D'
        states = self.HM.get_last()
        rxt = self.HM.refx[0].reshape((self.HM.refx[0].size,1)) # <---------------
        ryt = self.HM.ryt
        rut = self.HM.rut

        theta = np.vstack([states,rxt,ryt,rut]).transpose()

        f_obj = np.matmul(theta,self.HM.D.transpose()).astype('float')
        # print(f)
        # f_obj = [1.0, 2.0, 3.0, 1.0]
        self.P.variables.add(obj=f_obj.tolist()[0])


        # Add matrix for quadratic objective function (H):
        # self.P.objective.set_quadratic(qmat)
        (Hr,Hc) = np.nonzero(self.HM.H)
        H_vals = self.HM.H[(Hr,Hc)]
        self.P.objective.set_quadratic_coefficients(zip(Hr,Hc,H_vals))

        # Add vector for linear constraints (bineq & beq):
        # Define type of constraint GLER - greater than, less than, equal, ranged
        # Current problem only has inequalities ("L"). This value will be continuously updated in each iteration.
        # b = b0 + Cx*states
        b = self.HM.b0 + np.matmul(self.HM.Cx,states)
        senses = "L"*len(b)
        self.P.linear_constraints.add(rhs = b.transpose().tolist()[0], senses=senses)

        # Define linear constraints matrix (Aineq & Aeq):
        (Ar,Ac) = np.nonzero(self.HM.A)
        A_vals = self.HM.A[(Ar,Ac)]
        self.P.linear_constraints.set_coefficients(zip(Ar, Ac, A_vals))

        # Define variable's upper and lower bounds:
        # ub = [40.0, cplex.infinity, cplex.infinity, 3.0]
        # lb = [0.0, 0.0, 0.0, 0.0]
        #
        # Define variable types using CIBSN - continuous, integer, binary, semi-continuous, semi-integer:
        n_var = self.HM.A.shape[1]
        var_list = list("C"*n_var)
        for i in self.HM.ivar:
            var_list[i]="B"
        self.P.variables.set_types(zip(range(n_var),var_list))

    def updateoptim(self):
        # Update vector for linear objective function (f):
        # theta = [states;rx;ry;ru;rz]' (row vector)
        # D = matrix containing parameter linear costs
        # f = theta * D'
        states = self.HM.get_last()
        rxt = self.HM.refx[1].reshape((self.HM.refx[0].size, 1)) # <-----------
        ryt = self.HM.ryt
        rut = self.HM.rut

        theta = np.vstack([states, rxt, ryt, rut]).transpose()

        f_obj = np.matmul(theta, self.HM.D.transpose()).astype('float')
        # print(f)
        # f_obj = [1.0, 2.0, 3.0, 1.0]
        self.P.variables.add(obj=f_obj.tolist()[0])

        # Add vector for linear constraints (bineq & beq):
        # Define type of constraint GLER - greater than, less than, equal, ranged
        # Current problem only has inequalities ("L"). This value will be continuously updated in each iteration.
        # b = b0 + Cx*states
        b = self.HM.b0 + np.matmul(self.HM.Cx, states)
        senses = "L" * len(b)
        self.P.linear_constraints.add(rhs=b.transpose().tolist()[0], senses=senses)

    def solveiter(self):
        # Use this function to solve the current optimization problem after all the correspondent
        # iteration variables have been updated
        self.P.solve()
        sol = self.P.solution
        # print(sol.status[sol.get_status()])
        # Get the different variables from the optimization solution vector
        xmin = np.array(sol.get_values())
        u = xmin[self.HM.uvar[0:prob.HM.nu]-1].reshape(prob.HM.nu,1)
        d = xmin[self.HM.dvar[0:prob.HM.nd]-1].reshape(prob.HM.nd,1)
        z = xmin[self.HM.zvar[0:prob.HM.nz]-1].reshape(prob.HM.nz,1)
        x = np.matmul(self.HM.A1,self.HM.get_last())+np.matmul(self.HM.B1,u) + np.matmul(self.HM.B2,d) + np.matmul(self.HM.B3,z)

        return x
if __name__ == '__main__':
    try:
        prob = optimProb()
        prob.setproblemdata('gripper')


        print('Problem set. Starting:')
        # while not rospy.is_shutdown():
        while True:
            x = prob.solveiter()
            prob.HM.update_Hstates(x)
            print(prob.HM.get_last())
            prob.updateoptim()
            break
            try:
                input()
            except:
                pass
    # except rospy.ROSInterruptException:
    except:
        print('error')
        pass