function data = mat2py()
%% Global Parameters
Ts = 0.1;

addpath('hybridMPC')
f = xlsread('ref_map.xls');
ref_map = f(6:end,:);

% Gripper Parameters
k = -1.73; % Motor Gain
closed_theta = f(1,10); % Angle considered to be closed
open_theta = f(2,10); % Angle considered to be open

% Problem Parameters
grab_pos_x = f(1,2); % Grabbing point
grab_pos_y = f(1,3); % Grabbing point
grab_pos_z = f(1,4);

drop_pos_x = f(2,2);
drop_pos_y = f(2,3);
drop_pos_z = f(2,4);

end_pos_x = f(3,2);
end_pos_y = f(3,3);
end_pos_z = f(3,4);

safe_d = 1; % Distance Zone for the grabbing and dropping stops. MPCs are activated within these zones
v = 2; % Speed of the drone
vz = 0.15; % Vertical speed of the drone
safety_v = v; % Speed of the drone in the safety zone
kyaw = 0.5; % Yaw Gain
pre = -0.4;

safe_zone_x = (grab_pos_x-safe_d);
safe_zone_y = (grab_pos_y-safe_d);

% % Simulation Parameters
margin = 0.001; % Margin for the gripper to partially close before fully closing

%% Model Initiation
T_end = 50;
Nsim = round(T_end/Ts); % Number of samples in simulation

S=mld('HMPC2',Ts);

mldS = struct(S);
mld_struct = struct();

mld_struct.A1=mldS.A;
mld_struct.B1=mldS.B1;
mld_struct.B2=mldS.B2;
mld_struct.B3=mldS.B3;
mld_struct.C=mldS.C;
mld_struct.D1=mldS.D1;
mld_struct.D2=mldS.D2;
mld_struct.D3=mldS.D3;
mld_struct.E1=mldS.E1;

% System Dimensions
mld_struct.nu   = size(mld_struct.B1,2);
mld_struct.nd   = size(mld_struct.B2,2);
mld_struct.nz   = size(mld_struct.B3,2);
mld_struct.nx   = size(mld_struct.A1,1);
mld_struct.ny   = size(mld_struct.C,1);

%% MPC DESIGN
% Optimal Control Interval
N = 7; 
% N = (ceil(margin/(abs(k)*Ts)))*2+1;
% fprintf('Calculated N: %g\n',N)

% Inputs
u_max = 1; % Maximum input to the motor
refs.u = [1,2,3,4,5]; % u,ux,uy,uz,uyaw
Q.u=diag([20,1,1,1,1]);
limits.umin=[-u_max,-u_max,-u_max,-u_max,-u_max];           
limits.umax=[u_max,u_max,u_max,u_max,u_max]; 

% % State Variables
refs.x = [1,2,3,4,5,6,7,8,9,11];% theta,theta_dot,x,x_dot,y,y_dot,yaw,z,z_dot
Q.x = diag([50,1,30,1,30,1,50,1,1,10]);
Q.xN = diag([50,1,50,1,50,1,50,1,1,10]);

% Auxiliary Variables
% 1 - Weight for the dist calc[0-5]
% 2 - Weight penalty boolean variable !closed&passed [0-1]
% 3 - Weight closed before passed []
% 4 - Weight safe zone
% 5 - Weight block gripper
% 6 - Inter angle
refs.y=[2,3,4,5,6];
Q.y = diag([100,50,1,50,10]);

% Optimal problem parameters
Q.norm = 2;	 
Q.rho = 1;

% MPC controller            
MPC=hybcon(S,Q,N,limits,refs);

MPC_struct = struct(MPC);
MPC_struct = rmfield(MPC_struct,{'Y','mipsolver','name','pwa','hysmodel',...
    'Q','epsvar','refsignals','limits','model','Cr'});
if MPC_struct.norm ~= 2
    MPC_struct.CrX = MPC.Cr.x;
    MPC_struct.CrY = MPC.Cr.y;
    % MPC_struct.CrZ = MPC.Cr.z;
    MPC_struct.CrU = MPC.Cr.u;
end

MPC_struct.nry=length(MPC.refsignals.y);
MPC_struct.nrx=length(MPC.refsignals.x);
MPC_struct.nru=length(MPC.refsignals.u);
MPC_struct.nrz=length(MPC.refsignals.z);

%% Variable type definition
ctype=[];
for v = 1:size(MPC_struct.A,2)
    ctype=[ctype 'C'];          
end
ctype(MPC_struct.ivar) = 'B';

%% Phase Reference Map
% ref_map=[0,0,grab_pos_x,0,grab_pos_y,0,grab_pos_z,0,0,2;
%          closed_theta,0,grab_pos_x,0,grab_pos_y,0,grab_pos_z,0,0,3;
%          closed_theta,0,drop_pos_x,0,drop_pos_y,0,drop_pos_z,0,0,4;
%          -0.7,0,drop_pos_x,0,drop_pos_y,0,grab_pos_z,0,0,5;
%          -0.35,0,end_pos_x,0,end_pos_y,0,end_pos_z,0,0,5];

data.MPC = MPC_struct;
data.mld = mld_struct;
data.ref = ref_map;
data.ctype = ctype;

end

