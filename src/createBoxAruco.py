#!/usr/bin/env python

import numpy as np
from cv2 import aruco
import matplotlib.pyplot as plt
import matplotlib as mpl
import os
import math as m
from docx import Document

####################################################
############## Auxiliary Calculations ##############
####################################################

def centeroidnp(arr):
    """
    Calculates the centroid of a 4 points in space.
    :param arr:
    :return:
    """
    length = arr.shape[0]
    sum_x = np.sum(arr[:, 0])
    sum_y = np.sum(arr[:, 1])
    sum_z = np.sum(arr[:, 2])
    
    return sum_x/length, sum_y/length, sum_z/length
    
def geometric_transform(points,trans,rot):
    """
    Performs a geometric transformation of a shape in 3D space.
    :param points:
    :param trans:
    :param rot:
    :return:
    """
        
    Ttrans = np.float32([[1,0,0,0],
                       [0,1,0,0],
                        [0,0,1,0]])
    trans.append(1)
    Ttrans = np.insert(Ttrans,3,trans,0)
    
    r = [m.radians(x) for x in rot]

    Rx = np.float32([[1,0,0,0],
                   [0,m.cos(r[0]),-m.sin(r[0]),0],
                    [0,m.sin(r[0]),m.cos(r[0]),0],
                     [0,0,0,1]])
    Ry = np.float32([[m.cos(r[1]),0,m.sin(r[1]),0],
                    [0,1,0,0],
                    [-m.sin(r[1]),0,m.cos(r[1]),0],
                    [0,0,0,1]])
    Rz = np.float32([[m.cos(r[2]),-m.sin(r[2]),0,0],
                    [m.sin(r[2]),m.cos(r[2]),0,0],
                     [0,0,1,0],
                        [0,0,0,1]])
     
    Trot = np.dot(np.dot(Rx,Ry),Rz)                 
    
    T = np.dot(Trot,Ttrans)
     
    shape = np.shape(points)

    if (len(shape)==1):
        # One point with X dims  
        points_ = np.insert(points,3,1,axis = 0)
        transformed_total = np.dot(points_,T)
        
        return np.delete(transformed_total,3,0)
        
    elif (len(shape) == 2):
        # Y points with X dims
        points_ = np.insert(points,3,1,axis = 1)
        transformed_total = np.dot(points_,T)
        
        return np.delete(transformed_total,3,1)
        
    elif (len(shape) == 3):
        # Z groups of Y points with x dims
        transformed = []
        for p in points:
            points_ = np.insert(p,3,1,axis = 1)
            transformed_total = np.dot(points_,T)
            transformed.append(np.delete(transformed_total,3,1))
        return transformed

####################################################
################# Class Definition #################
####################################################

class markerBox():
    """
    twoDboard class

    Main class for generating the marker box object. Takes the dimensions of the board, and marker configuration and
    creates a box object.
    """
    def __init__(self,face_side,nr_markers,marker_separation,boxL=0,boxH=0,boxW=0):
        """
        :param face_side: length of the marker surface.
        :param nr_markers: needs to be the square of a natural number.
        :param marker_separation: space between markers
        :param boxL: box length
        :param boxH: box height
        :param boxW:box width
        """
        self.face_side = face_side
        self.marker_separation = marker_separation
        self.marker_side = (self.face_side-marker_separation*(m.sqrt(nr_markers)-1))/m.sqrt(nr_markers)
        self.nr_markers = nr_markers

        # Translation for each marker
        move = self.marker_side+self.marker_separation

        # Each box face will be defined individually. Although this is more exhaustive approach, it is more intuitive
        # and does not present any chance of error. Each face orientation is defined in the documentation.

        ## Face A -> XY at Z+
        # Step 1: the first marker of the current face is defined in accordance to the face orientation and considering
        # the origin at (0,0,0). The four corners are defined as [[x0,y0,z0],[x1,y1,z1],...]. T
        M1 = np.array([[0,self.face_side,0],
                       [self.marker_side,self.face_side,0],
                       [self.marker_side,self.face_side-self.marker_side,0],
                       [0,self.face_side-self.marker_side,0]]).astype('float32')
        # Step 2: Offset the marker to be placed in the correct position in the box.
        mX = (-face_side)/2
        mY = (-face_side)/2
        mZ = boxW / 2
        shift = np.array([mX,mY,mZ]).astype('float32')
        M1 = M1 + shift

        # Step 3: Perform the correct geometric transformation to replicate each marker corner points across the current
        # face. This part is only working for 4 or 9 marker per face. The geometric transformations are specific for
        # each face.
        self.corners = []
        self.corners.append(M1) # Marker 1
        self.corners.append(geometric_transform(M1,[move,0,0],[0,0,0])) # Marker 2
        self.corners.append(geometric_transform(M1,[2*move,0,0],[0,0,0])) # Marker 3   
        self.corners.append(geometric_transform(M1,[0,-move,0],[0,0,0])) # Marker 4
        self.corners.append(geometric_transform(M1,[move,-move,0],[0,0,0])) # Marker 5
        self.corners.append(geometric_transform(M1,[2*move,-move,0],[0,0,0])) # Marker 6
        self.corners.append(geometric_transform(M1,[0,-2*move,0],[0,0,0])) # Marker 7
        self.corners.append(geometric_transform(M1,[move,-2*move,0],[0,0,0])) # Marker 8
        self.corners.append(geometric_transform(M1,[2*move,-2*move,0],[0,0,0])) # Marker 9

        ## Face B -> ZY at X-
        M4 = np.array([[0,self.face_side,self.face_side],
                       [0,self.face_side-self.marker_side,self.face_side],
                        [0,self.face_side-self.marker_side,self.face_side-self.marker_side],
                       [0,self.face_side,self.face_side-self.marker_side]]).astype('float32')
                       
        mY = (-face_side)/2
        mZ = (-face_side)/2
        mX = -boxH / 2
        shift = np.array([mX,mY,mZ]).astype('float32')
        M4 = M4 + shift

        self.corners.append(M4) # Marker 1
        self.corners.append(geometric_transform(M4,[0,-move,0],[0,0,0])) # Marker 2
        self.corners.append(geometric_transform(M4,[0,-2*move,0],[0,0,0])) # Marker 3   
        self.corners.append(geometric_transform(M4,[0,0,-move],[0,0,0])) # Marker 4
        self.corners.append(geometric_transform(M4,[0,-move,-move],[0,0,0])) # Marker 5
        self.corners.append(geometric_transform(M4,[0,-2*move,-move],[0,0,0])) # Marker 6
        self.corners.append(geometric_transform(M4,[0,0,-2*move],[0,0,0])) # Marker 7
        self.corners.append(geometric_transform(M4,[0,-move,-2*move],[0,0,0])) # Marker 8
        self.corners.append(geometric_transform(M4,[0,-2*move,-2*move],[0,0,0])) # Marker 9

        ## Face C -> XZ at Y+
        M2 = np.array([[0,0,self.face_side],
                       [0,0,self.face_side-self.marker_side],
                        [self.marker_side,0,self.face_side-self.marker_side],
                       [self.marker_side,0,self.face_side]]).astype('float32')
                       
        mX = (-face_side)/2
        mZ = (-face_side)/2
        mY = boxL / 2
        shift = np.array([mX,mY,mZ]).astype('float32')
        M2 = M2 + shift
        
        self.corners.append(M2) # Marker 1
        self.corners.append(geometric_transform(M2,[0,0,-move],[0,0,0])) # Marker 2
        self.corners.append(geometric_transform(M2,[0,0,-2*move],[0,0,0])) # Marker 3   
        self.corners.append(geometric_transform(M2,[move,0,0],[0,0,0])) # Marker 4
        self.corners.append(geometric_transform(M2,[move,0,-move],[0,0,0])) # Marker 5
        self.corners.append(geometric_transform(M2,[move,0,-2*move],[0,0,0])) # Marker 6
        self.corners.append(geometric_transform(M2,[2*move,0,0],[0,0,0])) # Marker 7
        self.corners.append(geometric_transform(M2,[2*move,0,-move],[0,0,0])) # Marker 8
        self.corners.append(geometric_transform(M2,[2*move,0,-2*move],[0,0,0])) # Marker 9

        ## Face D -> ZY at X+
        M3 = np.array([[0,0,self.face_side],
                       [0,self.marker_side,self.face_side],
                        [0,self.marker_side,self.face_side-self.marker_side],
                       [0,0,self.face_side-self.marker_side]]).astype('float32')
                       
        mY = (-face_side)/2
        mZ = (-face_side)/2
        mX = boxH / 2
        shift = np.array([mX,mY,mZ]).astype('float32')
        M3 = M3 + shift

        self.corners.append(M3) # Marker 1
        self.corners.append(geometric_transform(M3,[0,move,0],[0,0,0])) # Marker 2
        self.corners.append(geometric_transform(M3,[0,2*move,0],[0,0,0])) # Marker 3   
        self.corners.append(geometric_transform(M3,[0,0,-move],[0,0,0])) # Marker 4
        self.corners.append(geometric_transform(M3,[0,move,-move],[0,0,0])) # Marker 5
        self.corners.append(geometric_transform(M3,[0,2*move,-move],[0,0,0])) # Marker 6
        self.corners.append(geometric_transform(M3,[0,0,-2*move],[0,0,0])) # Marker 7
        self.corners.append(geometric_transform(M3,[0,move,-2*move],[0,0,0])) # Marker 8
        self.corners.append(geometric_transform(M3,[0,2*move,-2*move],[0,0,0])) # Marker 9
        
        ###### Face E -> > XZ at Y- ########----------------------
        M6 = np.array([[self.face_side,0,self.face_side],
               [self.face_side,0,self.face_side-self.marker_side],
               [self.face_side-self.marker_side,0,self.face_side-self.marker_side],
               [self.face_side-self.marker_side,0,self.face_side]]).astype('float32')
                       
        mX = (-face_side)/2
        mZ = (-face_side)/2
        mY = -boxL / 2
        shift = np.array([mX,mY,mZ]).astype('float32')
        M6 = M6 + shift
        
        self.corners.append(M6) # Marker 1
        self.corners.append(geometric_transform(M6,[0,0,-move],[0,0,0])) # Marker 2
        self.corners.append(geometric_transform(M6,[0,0,-2*move],[0,0,0])) # Marker 3   
        self.corners.append(geometric_transform(M6,[-move,0,0],[0,0,0])) # Marker 4
        self.corners.append(geometric_transform(M6,[-move,0,-move],[0,0,0])) # Marker 5
        self.corners.append(geometric_transform(M6,[-move,0,-2*move],[0,0,0])) # Marker 6
        self.corners.append(geometric_transform(M6,[-2*move,0,0],[0,0,0])) # Marker 7
        self.corners.append(geometric_transform(M6,[-2*move,0,-move],[0,0,0])) # Marker 8
        self.corners.append(geometric_transform(M6,[-2*move,0,-2*move],[0,0,0])) # Marker 9
        
        ##### Face F -> XY at Z- ##############---------------------
        M5 = np.array([[self.face_side,self.face_side,0],
               [self.face_side-self.marker_side,self.face_side,0],
               [self.face_side-self.marker_side,self.face_side-self.marker_side,0],
               [self.face_side,self.face_side-self.marker_side,0]]).astype('float32')
                       
        mY = (-face_side)/2
        mX = (-face_side)/2
        mZ = -boxW / 2
        shift = np.array([mX,mY,mZ]).astype('float32')
        M5 = M5 + shift
        
        self.corners.append(M5) # Marker 1
        self.corners.append(geometric_transform(M5,[-move,0,0],[0,0,0])) # Marker 2
        self.corners.append(geometric_transform(M5,[-2*move,0,0],[0,0,0])) # Marker 3   
        self.corners.append(geometric_transform(M5,[0,-move,0],[0,0,0])) # Marker 4
        self.corners.append(geometric_transform(M5,[-move,-move,0],[0,0,0])) # Marker 5
        self.corners.append(geometric_transform(M5,[-2*move,-move,0],[0,0,0])) # Marker 6
        self.corners.append(geometric_transform(M5,[0,-2*move,0],[0,0,0])) # Marker 7
        self.corners.append(geometric_transform(M5,[-move,-2*move,0],[0,0,0])) # Marker 8
        self.corners.append(geometric_transform(M5,[-2*move,-2*move,0],[0,0,0])) # Marker 9

        # Assign an id for each marker. Right now all box are being generated with the same sequence of ids, making
        # them undistinguishable.
        self.board_ids = np.zeros([6*self.nr_markers,1])
        idx = 0
        for i,marker in enumerate(self.corners):
            self.board_ids[i] = idx
            idx += 1

        self.aruco_dict = aruco.getPredefinedDictionary(aruco.DICT_6X6_250)
        self.aruco_board = aruco.Board_create(self.corners,self.aruco_dict,self.board_ids)
        
    def printFaces(self):
        """
        Save each face to a file and add them together in a document in the right dimensions, to facilitate their
        printing and implementation.
        """
        Faces = Document()
        Ids = Document()

        # Create a folder named "makers" if it doesn't exist
        if not os.path.exists('markers'):
            os.makedirs('markers')

        # Add box characteristics
        p1 = Ids.add_paragraph()
        r1 = p1.add_run()
        r1.add_text("Face Side: "+str(self.face_side) + " | ")
        r1.add_text("Marker Side: "+str(self.marker_side) + " | ")
        p1 = Ids.add_paragraph()
        r1 = p1.add_run()
        r1.add_text("Nr. Markers: "+str(self.nr_markers) + " | ")
        r1.add_text("Marker Space: "+str(self.marker_separation) + " | ")
        
        plt.ioff()
        to_inch = 0.0254
        space = self.marker_separation/to_inch
        s = m.sqrt(self.nr_markers)

        # Save and copy each face
        for face in range(1,7):
            offset = cube.nr_markers*(face-1)
            
            ## Save each face
            fig = plt.figure(figsize=(round(self.face_side/to_inch,3), round(self.face_side/to_inch,3)),
                             edgecolor = (0,0,0),frameon = True)        
            offset = self.nr_markers*(face-1)
            for j in range(self.nr_markers):
                ax = plt.subplot(s,s,j+1)
                img = aruco.drawMarker(self.aruco_dict,self.board_ids[j+offset], 100)
                ax.imshow(img, cmap = mpl.cm.gray, interpolation = "nearest")
                plt.axis('off')

            fig.subplots_adjust(bottom =0, top = 1, left=0, right=1,wspace = space,hspace = space) 
            name = "markers_F{}".format(face)
            bb = self.marker_separation/(to_inch)
            bbox = mpl.transforms.Bbox([[-bb, -bb],[fig.get_figwidth()+bb, fig.get_figheight()+bb]])
            fig.savefig("markers/"+name+".png",format = 'png',dpi=100,transparent = False,
                        frameon = True, edgecolor = (0,1,0),bbox_inches= bbox)
            
            p = Faces.add_paragraph()
            r = p.add_run()
            r.add_text("Face "+str(face))
            r.add_picture('markers/markers_F'+str(face)+'.png')

            # Add face to the document
            p1 = Ids.add_paragraph()
            r1 = p1.add_run()
            r1.add_text("Face "+str(face))
            r1.bold = True
            s = int(m.sqrt(cube.nr_markers))
            table = Ids.add_table(rows=s, cols=s)
            table.allow_autofit = True
            c = 0
            for row in table.rows:
                for cell in row.cells:
                    cell.text = str(int(cube.board_ids[c+offset][0]))
                    c += 1
            Ids.add_paragraph()

            print("Face "+str(face)+" is saved.")
        plt.close('all')
        Faces.save('markers/Faces.docx')
        Ids.save('markers/Ids.docx')

        # It was not possible to implement this in this code, but it would also be beneficial to draw a border around
        # each face, to make cutting the paper easier and have straighter faces.
        

        
                    
