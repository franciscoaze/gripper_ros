#!/usr/bin/env python
import rospy
import serial
from pytests.msg import ArduinoInfo
import sys


class arduino_receive:
    """
    arduino_receive class
    Communicates with the Arduino via Serial port and publishes relevant data to a ROS topic.
    """
    def __init__(self):
        # Initialize ROS node and ROS publishers
        rospy.init_node('arduino_receive',anonymous=True)   
        self.pub = rospy.Publisher('arduino_info', ArduinoInfo, queue_size=10)
        rospy.sleep(0.1)

        self.send_msg = ArduinoInfo()
        # Try different ports to initialize the serial connection.
        try:
            self.ser = serial.Serial('/dev/ttyUSB0', 115200,timeout=0.5)
        except:
            try:
                self.ser = serial.Serial('/dev/ttyUSB1', 115200,timeout=0.5)
            except:
                self.send_msg.status = -1
                self.pub.publish(self.send_msg)
                rospy.signal_shutdown('Arduino not connected')
                sys.exit()

        # When serial communication is achieved, send a status message of 1 to the main node.
        self.send_msg.status = 1
        self.pub.publish(self.send_msg)

        # Start listening to serial messages.
        self.get_arduino_message()


    def get_arduino_message(self):
        """
        Listens to the serial port and waits for a suitable message. Publishes the relevant data to a specified topic
        """
        # Runs the loop no faster than 100 hz. The serial port timeout was specified when the port was initialized.
        r = rospy.Rate(100)
        while not rospy.is_shutdown():
            message = self.ser.readline()
            # print('[Arduino]: {}'.format(message))
            msg_list = message.split(',')
            if len(msg_list)>3:
                self.send_msg.current_threshold = int(msg_list[0])
                self.send_msg.gripper_angle = int(msg_list[1])
                self.send_msg.current = int(msg_list[2])
                self.send_msg.current_stall = int(msg_list[3])
                self.send_msg.header.stamp = rospy.Time.now()
                self.pub.publish(self.send_msg)
            elif(len(msg_list)==1):
                if('OK' in msg_list[0]):
                    # Publish status message indicating that the gripper is homed.
                    self.send_msg.status = 2
                    self.pub.publish(self.send_msg)
            self.pub.publish(self.send_msg)
            r.sleep() 

if __name__ == '__main__':
    try:
        d = arduino_receive()
        rospy.spin()
    except rospy.ROSInterruptException:
        pass



