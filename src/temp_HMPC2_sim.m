function [xn, d, z, y] = temp_HMPC2_sim(x, u, params)
% [xn, d, z, y] = temp_HMPC2_sim(x, u, params)
% simulates the hybrid system one step ahead.
% Parameters:
%   x: current state
%   u: input
%   params: structure containing values for
%           all symbolic parameters
% Output:
%   xn: state in the next timestep
%   u: output
%   d, z: Boolean and real auxiliary variables
%
% HYSDEL 2.0.5 (Build: 20111112)
% Copyright (C) 1999-2002  Fabio D. Torrisi
% 
% HYSDEL comes with ABSOLUTELY NO WARRANTY;
% HYSDEL is free software; you can redistribute it and/or
% modify it under the terms of the GNU General Public
% License as published by the Free Software Foundation; either
% version 2 of the License, or (at your option) any later version.
if ~exist('params', 'var')
	error('error: params not available');
end
if ~isa(params, 'struct')
	error('error: params is not a struct');
end
if ~isfield(params, 'Ts')
	error('error: symbolic parameter Ts not defined in params structure');
end
if ~isfield(params, 'closed_theta')
	error('error: symbolic parameter closed_theta not defined in params structure');
end
if ~isfield(params, 'drop_pos_x')
	error('error: symbolic parameter drop_pos_x not defined in params structure');
end
if ~isfield(params, 'drop_pos_y')
	error('error: symbolic parameter drop_pos_y not defined in params structure');
end
if ~isfield(params, 'drop_pos_z')
	error('error: symbolic parameter drop_pos_z not defined in params structure');
end
if ~isfield(params, 'grab_pos_x')
	error('error: symbolic parameter grab_pos_x not defined in params structure');
end
if ~isfield(params, 'grab_pos_y')
	error('error: symbolic parameter grab_pos_y not defined in params structure');
end
if ~isfield(params, 'grab_pos_z')
	error('error: symbolic parameter grab_pos_z not defined in params structure');
end
if ~isfield(params, 'k')
	error('error: symbolic parameter k not defined in params structure');
end
if ~isfield(params, 'kyaw')
	error('error: symbolic parameter kyaw not defined in params structure');
end
if ~isfield(params, 'margin')
	error('error: symbolic parameter margin not defined in params structure');
end
if ~isfield(params, 'pre')
	error('error: symbolic parameter pre not defined in params structure');
end
if ~isfield(params, 'safe_zone_x')
	error('error: symbolic parameter safe_zone_x not defined in params structure');
end
if ~isfield(params, 'safe_zone_y')
	error('error: symbolic parameter safe_zone_y not defined in params structure');
end
if ~isfield(params, 'safety_v')
	error('error: symbolic parameter safety_v not defined in params structure');
end
if ~isfield(params, 'v')
	error('error: symbolic parameter v not defined in params structure');
end
if ~isfield(params, 'vz')
	error('error: symbolic parameter vz not defined in params structure');
end
if ~exist('x', 'var')
	error('error:  current state x not supplied');
end
x=x(:);
if ~all (size(x)==[12 1])
	error('error: state vector has wrong dimension');
end
if ~exist('u', 'var')
	error('error: input u not supplied');
end
u=u(:);
if ~all (size(u)==[5 1])
	error('error: input vector has wrong dimension');
end

d = zeros(42, 1);
z = zeros(16, 1);
xn = zeros(12, 1);
y = zeros(6, 1);

if (x(12) < 0) | (x(12) > 1)
	error('variable fclosed is out of bounds');
end
if (x(11) < 1) | (x(11) > 10)
	error('variable ph is out of bounds');
end
if (x(1) < -1.5) | (x(1) > 1.5)
	error('variable theta is out of bounds');
end
if (x(2) < -5) | (x(2) > 5)
	error('variable theta_dot is out of bounds');
end
if (u(1) < -1) | (u(1) > 1)
	error('variable u is out of bounds');
end
if (u(2) < -1) | (u(2) > 1)
	error('variable ux is out of bounds');
end
if (u(3) < -1) | (u(3) > 1)
	error('variable uy is out of bounds');
end
if (u(5) < -1) | (u(5) > 1)
	error('variable uyaw is out of bounds');
end
if (u(4) < -1) | (u(4) > 1)
	error('variable uz is out of bounds');
end
if (x(3) < -100) | (x(3) > 100)
	error('variable x is out of bounds');
end
if (x(4) < -5) | (x(4) > 5)
	error('variable x_dot is out of bounds');
end
if (x(5) < -100) | (x(5) > 100)
	error('variable y is out of bounds');
end
if (x(6) < -5) | (x(6) > 5)
	error('variable y_dot is out of bounds');
end
if (x(9) < -10) | (x(9) > 10)
	error('variable yaw is out of bounds');
end
if (x(10) < -10) | (x(10) > 10)
	error('variable yaw_dot is out of bounds');
end
if (x(7) < -10) | (x(7) > 10)
	error('variable z is out of bounds');
end
if (x(8) < -5) | (x(8) > 5)
	error('variable z_dot is out of bounds');
end

% closed_aux = theta - 0.001 <= closed_theta;
within(((x(1)) - (0.001)) - (params.closed_theta), min((-1.5) + ((-0.001) + (-params.closed_theta)), (1.5) + ((-0.001) + (-params.closed_theta))), max((1.5) + ((-0.001) + (-params.closed_theta)), (-1.5) + ((-0.001) + (-params.closed_theta))), 112);
if ((x(1)) - (0.001)) - (params.closed_theta) <= 0
	d(35) = 1;
else
	d(35) = 0;
end

% closed_margin = theta - margin <= closed_theta;
within(((x(1)) - (params.margin)) - (params.closed_theta), min((-1.5) + ((-params.margin) + (-params.closed_theta)), (1.5) + ((-params.margin) + (-params.closed_theta))), max((1.5) + ((-params.margin) + (-params.closed_theta)), (-1.5) + ((-params.margin) + (-params.closed_theta))), 113);
if ((x(1)) - (params.margin)) - (params.closed_theta) <= 0
	d(22) = 1;
else
	d(22) = 0;
end

% dtheta = -k * u;
z(4) = (-params.k) * (u(1));

% dvx = ux * v;
z(5) = (u(2)) * (params.v);

% dvy = uy * v;
z(6) = (u(3)) * (params.v);

% dvz = uz * vz;
z(7) = (u(4)) * (params.vz);

% dyaw = kyaw * uyaw;
z(8) = (params.kyaw) * (u(5));

% isPH1 = ph <= 1.1;
within((x(11)) - (1.1), -0.1, 8.9, 101);
if (x(11)) - (1.1) <= 0
	d(25) = 1;
else
	d(25) = 0;
end

% isPH2_high = ph <= 2.9;
within((x(11)) - (2.9), -1.9, 7.1, 104);
if (x(11)) - (2.9) <= 0
	d(26) = 1;
else
	d(26) = 0;
end

% isPH2_low = ph >= 1.9;
within((1.9) - (x(11)), -8.1, 0.9, 103);
if (1.9) - (x(11)) <= 0
	d(27) = 1;
else
	d(27) = 0;
end

% isPH3_high = ph <= 3.9;
within((x(11)) - (3.9), -2.9, 6.1, 107);
if (x(11)) - (3.9) <= 0
	d(29) = 1;
else
	d(29) = 0;
end

% isPH3_low = ph >= 2.9;
within((2.9) - (x(11)), -7.1, 1.9, 106);
if (2.9) - (x(11)) <= 0
	d(30) = 1;
else
	d(30) = 0;
end

% isPH4_high = ph <= 4.9;
within((x(11)) - (4.9), -3.9, 5.1, 110);
if (x(11)) - (4.9) <= 0
	d(34) = 1;
else
	d(34) = 0;
end

% isPH4_low = ph >= 3.95;
within((3.95) - (x(11)), -6.05, 2.95, 109);
if (3.95) - (x(11)) <= 0
	d(33) = 1;
else
	d(33) = 0;
end

% on_drop_x = drop_pos_x - 0.01 <= x;
within(((params.drop_pos_x) - (0.01)) - (x(3)), min(((0) + ((params.drop_pos_x) + (-0.01))) + (-100), ((0) + ((params.drop_pos_x) + (-0.01))) + (100)), max(((0) + ((params.drop_pos_x) + (-0.01))) + (100), ((0) + ((params.drop_pos_x) + (-0.01))) + (-100)), 97);
if ((params.drop_pos_x) - (0.01)) - (x(3)) <= 0
	d(10) = 1;
else
	d(10) = 0;
end

% on_drop_y = drop_pos_y - 0.01 <= y;
within(((params.drop_pos_y) - (0.01)) - (x(5)), min(((0) + ((params.drop_pos_y) + (-0.01))) + (-100), ((0) + ((params.drop_pos_y) + (-0.01))) + (100)), max(((0) + ((params.drop_pos_y) + (-0.01))) + (100), ((0) + ((params.drop_pos_y) + (-0.01))) + (-100)), 98);
if ((params.drop_pos_y) - (0.01)) - (x(5)) <= 0
	d(16) = 1;
else
	d(16) = 0;
end

% on_drop_z = drop_pos_z - 0.01 <= z;
within(((params.drop_pos_z) - (0.01)) - (x(7)), min(((0) + ((params.drop_pos_z) + (-0.01))) + (-10), ((0) + ((params.drop_pos_z) + (-0.01))) + (10)), max(((0) + ((params.drop_pos_z) + (-0.01))) + (10), ((0) + ((params.drop_pos_z) + (-0.01))) + (-10)), 99);
if ((params.drop_pos_z) - (0.01)) - (x(7)) <= 0
	d(20) = 1;
else
	d(20) = 0;
end

% passed_margin_xH = (grab_pos_x + 0.01) >= x;
within((x(3)) - ((params.grab_pos_x) + (0.01)), min((-100) + (-((params.grab_pos_x) + (0.01))), (100) + (-((params.grab_pos_x) + (0.01)))), max((100) + (-((params.grab_pos_x) + (0.01))), (-100) + (-((params.grab_pos_x) + (0.01)))), 85);
if (x(3)) - ((params.grab_pos_x) + (0.01)) <= 0
	d(8) = 1;
else
	d(8) = 0;
end

% passed_margin_xL = (grab_pos_x - 0.01) <= x;
within(((params.grab_pos_x) - (0.01)) - (x(3)), min(((0) + ((params.grab_pos_x) + (-0.01))) + (-100), ((0) + ((params.grab_pos_x) + (-0.01))) + (100)), max(((0) + ((params.grab_pos_x) + (-0.01))) + (100), ((0) + ((params.grab_pos_x) + (-0.01))) + (-100)), 84);
if ((params.grab_pos_x) - (0.01)) - (x(3)) <= 0
	d(7) = 1;
else
	d(7) = 0;
end

% passed_margin_yH = (grab_pos_y + 0.01) >= y;
within((x(5)) - ((params.grab_pos_y) + (0.01)), min((-100) + (-((params.grab_pos_y) + (0.01))), (100) + (-((params.grab_pos_y) + (0.01)))), max((100) + (-((params.grab_pos_y) + (0.01))), (-100) + (-((params.grab_pos_y) + (0.01)))), 90);
if (x(5)) - ((params.grab_pos_y) + (0.01)) <= 0
	d(14) = 1;
else
	d(14) = 0;
end

% passed_margin_yL = (grab_pos_y - 0.01) <= y;
within(((params.grab_pos_y) - (0.01)) - (x(5)), min(((0) + ((params.grab_pos_y) + (-0.01))) + (-100), ((0) + ((params.grab_pos_y) + (-0.01))) + (100)), max(((0) + ((params.grab_pos_y) + (-0.01))) + (100), ((0) + ((params.grab_pos_y) + (-0.01))) + (-100)), 89);
if ((params.grab_pos_y) - (0.01)) - (x(5)) <= 0
	d(13) = 1;
else
	d(13) = 0;
end

% passed_x = (grab_pos_x) <= x;
within((params.grab_pos_x) - (x(3)), min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 83);
if (params.grab_pos_x) - (x(3)) <= 0
	d(5) = 1;
else
	d(5) = 0;
end

% passed_y = (grab_pos_y) <= y;
within((params.grab_pos_y) - (x(5)), min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 88);
if (params.grab_pos_y) - (x(5)) <= 0
	d(11) = 1;
else
	d(11) = 0;
end

% passed_zH = (grab_pos_z - 0.01) <= z;
within(((params.grab_pos_z) - (0.01)) - (x(7)), min(((0) + ((params.grab_pos_z) + (-0.01))) + (-10), ((0) + ((params.grab_pos_z) + (-0.01))) + (10)), max(((0) + ((params.grab_pos_z) + (-0.01))) + (10), ((0) + ((params.grab_pos_z) + (-0.01))) + (-10)), 95);
if ((params.grab_pos_z) - (0.01)) - (x(7)) <= 0
	d(18) = 1;
else
	d(18) = 0;
end

% passed_zL = (grab_pos_z + 0.01) >= z;
within((x(7)) - ((params.grab_pos_z) + (0.01)), min((-10) + (-((params.grab_pos_z) + (0.01))), (10) + (-((params.grab_pos_z) + (0.01)))), max((10) + (-((params.grab_pos_z) + (0.01))), (-10) + (-((params.grab_pos_z) + (0.01)))), 94);
if (x(7)) - ((params.grab_pos_z) + (0.01)) <= 0
	d(17) = 1;
else
	d(17) = 0;
end

% passed_zone_x = (safe_zone_x) <= x;
within((params.safe_zone_x) - (x(3)), min(((0) + (params.safe_zone_x)) + (-100), ((0) + (params.safe_zone_x)) + (100)), max(((0) + (params.safe_zone_x)) + (100), ((0) + (params.safe_zone_x)) + (-100)), 86);
if (params.safe_zone_x) - (x(3)) <= 0
	d(6) = 1;
else
	d(6) = 0;
end

% passed_zone_y = (safe_zone_y) <= y;
within((params.safe_zone_y) - (x(5)), min(((0) + (params.safe_zone_y)) + (-100), ((0) + (params.safe_zone_y)) + (100)), max(((0) + (params.safe_zone_y)) + (100), ((0) + (params.safe_zone_y)) + (-100)), 91);
if (params.safe_zone_y) - (x(5)) <= 0
	d(12) = 1;
else
	d(12) = 0;
end

% prepared = theta >= pre;
within((params.pre) - (x(1)), min(((0) + (params.pre)) + (-1.5), ((0) + (params.pre)) + (1.5)), max(((0) + (params.pre)) + (1.5), ((0) + (params.pre)) + (-1.5)), 114);
if (params.pre) - (x(1)) <= 0
	d(23) = 1;
else
	d(23) = 0;
end

% closed = closed_margin;
d(21) = d(22);

% isPH2 = isPH2_low & isPH2_high;
d(28) = (d(27)) & (d(26));

% isPH3 = isPH3_low & isPH3_high;
d(31) = (d(30)) & (d(29));

% isPH4 = isPH4_low & isPH4_high;
d(32) = (d(33)) & (d(34));

% on_drop = on_drop_x & on_drop_y & on_drop_z;
d(4) = ((d(10)) & (d(16))) & (d(20));

% passed = passed_x & passed_y;
d(1) = (d(5)) & (d(11));

% passed_margin_x = passed_margin_xL & passed_margin_xH;
d(9) = (d(7)) & (d(8));

% passed_margin_y = passed_margin_yL & passed_margin_yH;
d(15) = (d(13)) & (d(14));

% passed_margin_z = (passed_zL & passed_zH);
d(19) = (d(17)) & (d(18));

% passed_zone = passed_zone_x && passed_zone_y;
d(2) = (d(6)) & (d(12));

% safe_vel_x = {IF passed_zone & isPH2 THEN x_dot ELSE 0};
d(38) = (d(2)) & (d(28));

% safe_vel_y = {IF passed_zone & isPH2 THEN y_dot ELSE 0};
d(39) = (d(2)) & (d(28));

% block_gripper = {IF isPH3 | isPH1 THEN u ELSE 0};
d(40) = (d(31)) | (d(25));

% inter = !closed_aux & (!prepared) & isPH2;
d(42) = ((~d(35)) & (~d(23))) & (d(28));

% dist_calc_x = {IF !passed THEN grab_pos_x - x ELSE 0};
if d(1)
	within(0, 0, 0, 122);
	z(12) = 0;
else
	within((params.grab_pos_x) - (x(3)), min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 122);
	z(12) = (params.grab_pos_x) - (x(3));
end

% dist_calc_y = {IF !passed THEN grab_pos_y - y ELSE 0};
if d(1)
	within(0, 0, 0, 123);
	z(13) = 0;
else
	within((params.grab_pos_y) - (x(5)), min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 123);
	z(13) = (params.grab_pos_y) - (x(5));
end

% passed_margin = passed_margin_x & passed_margin_y & passed_margin_z;
d(3) = ((d(9)) & (d(15))) & (d(19));

% pen_aux = !closed & passed & (isPH2 | isPH3);
d(24) = ((~d(21)) & (d(1))) & ((d(28)) | (d(31)));

% zone_u = {IF !passed_zone THEN u ELSE 0};
if d(2)
	within(0, 0, 0, 129);
	z(11) = 0;
else
	within(u(1), -1, 1, 129);
	z(11) = u(1);
end

% block_gripper = {IF isPH3 | isPH1 THEN u ELSE 0};
if d(40)
	within(u(1), -1, 1, 134);
	z(3) = u(1);
else
	within(0, 0, 0, 134);
	z(3) = 0;
end

% closed_dist_x = {IF closed_aux THEN dist_calc_x ELSE 0};
if d(35)
	within(z(12), min((0) + (min((1) * (min(min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0), max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0), min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0))))), (0) + (max((1) * (min(min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0), max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0), min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0)))))), max((0) + (max((1) * (min(min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0), max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0), min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0))))), (0) + (min((1) * (min(min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0), max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0), min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0)))))), 124);
	z(14) = z(12);
else
	within(0, 0, 0, 124);
	z(14) = 0;
end

% closed_dist_y = {IF closed_aux THEN dist_calc_y ELSE 0};
if d(35)
	within(z(13), min((0) + (min((1) * (min(min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0), max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0), min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0))))), (0) + (max((1) * (min(min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0), max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0), min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0)))))), max((0) + (max((1) * (min(min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0), max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0), min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0))))), (0) + (min((1) * (min(min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0), max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0), min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0)))))), 125);
	z(15) = z(13);
else
	within(0, 0, 0, 125);
	z(15) = 0;
end

% safe_vel_x = {IF passed_zone & isPH2 THEN x_dot ELSE 0};
if d(38)
	within(x(4), -5, 5, 131);
	z(1) = x(4);
else
	within(0, 0, 0, 131);
	z(1) = 0;
end

% safe_vel_y = {IF passed_zone & isPH2 THEN y_dot ELSE 0};
if d(39)
	within(x(6), -5, 5, 132);
	z(2) = x(6);
else
	within(0, 0, 0, 132);
	z(2) = 0;
end

% secured = {IF fclosed THEN 1 ELSE (REAL passed_margin)};
if x(12)
	within(1, 1, 1, 136);
	z(16) = 1;
else
	within(d(3), 0, 1, 136);
	z(16) = d(3);
end

% cl_dst = closed_dist_x >= closed_dist_y;
within((z(15)) - (z(14)), min(((0) + (min((1) * (min(min(min((0) + (min((1) * (min(min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0), max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0), min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0))))), (0) + (max((1) * (min(min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0), max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0), min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0)))))), 0), max(max((0) + (max((1) * (min(min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0), max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0), min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0))))), (0) + (min((1) * (min(min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0), max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0), min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0)))))), 0))), (1) * (max(max(max((0) + (max((1) * (min(min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0), max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0), min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0))))), (0) + (min((1) * (min(min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0), max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0), min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0)))))), 0), min(min((0) + (min((1) * (min(min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0), max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0), min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0))))), (0) + (max((1) * (min(min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0), max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0), min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0)))))), 0)))))) + (min((-1) * (min(min(min((0) + (min((1) * (min(min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0), max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0), min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0))))), (0) + (max((1) * (min(min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0), max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0), min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0)))))), 0), max(max((0) + (max((1) * (min(min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0), max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0), min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0))))), (0) + (min((1) * (min(min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0), max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0), min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0)))))), 0))), (-1) * (max(max(max((0) + (max((1) * (min(min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0), max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0), min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0))))), (0) + (min((1) * (min(min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0), max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0), min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0)))))), 0), min(min((0) + (min((1) * (min(min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0), max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0), min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0))))), (0) + (max((1) * (min(min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0), max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0), min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0)))))), 0))))), ((0) + (max((1) * (min(min(min((0) + (min((1) * (min(min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0), max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0), min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0))))), (0) + (max((1) * (min(min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0), max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0), min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0)))))), 0), max(max((0) + (max((1) * (min(min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0), max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0), min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0))))), (0) + (min((1) * (min(min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0), max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0), min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0)))))), 0))), (1) * (max(max(max((0) + (max((1) * (min(min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0), max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0), min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0))))), (0) + (min((1) * (min(min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0), max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0), min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0)))))), 0), min(min((0) + (min((1) * (min(min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0), max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0), min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0))))), (0) + (max((1) * (min(min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0), max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0), min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0)))))), 0)))))) + (max((-1) * (min(min(min((0) + (min((1) * (min(min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0), max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0), min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0))))), (0) + (max((1) * (min(min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0), max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0), min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0)))))), 0), max(max((0) + (max((1) * (min(min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0), max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0), min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0))))), (0) + (min((1) * (min(min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0), max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0), min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0)))))), 0))), (-1) * (max(max(max((0) + (max((1) * (min(min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0), max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0), min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0))))), (0) + (min((1) * (min(min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0), max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0), min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0)))))), 0), min(min((0) + (min((1) * (min(min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0), max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0), min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0))))), (0) + (max((1) * (min(min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0), max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0), min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0)))))), 0)))))), max(((0) + (max((1) * (min(min(min((0) + (min((1) * (min(min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0), max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0), min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0))))), (0) + (max((1) * (min(min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0), max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0), min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0)))))), 0), max(max((0) + (max((1) * (min(min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0), max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0), min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0))))), (0) + (min((1) * (min(min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0), max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0), min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0)))))), 0))), (1) * (max(max(max((0) + (max((1) * (min(min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0), max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0), min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0))))), (0) + (min((1) * (min(min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0), max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0), min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0)))))), 0), min(min((0) + (min((1) * (min(min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0), max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0), min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0))))), (0) + (max((1) * (min(min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0), max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0), min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0)))))), 0)))))) + (max((-1) * (min(min(min((0) + (min((1) * (min(min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0), max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0), min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0))))), (0) + (max((1) * (min(min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0), max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0), min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0)))))), 0), max(max((0) + (max((1) * (min(min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0), max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0), min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0))))), (0) + (min((1) * (min(min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0), max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0), min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0)))))), 0))), (-1) * (max(max(max((0) + (max((1) * (min(min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0), max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0), min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0))))), (0) + (min((1) * (min(min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0), max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0), min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0)))))), 0), min(min((0) + (min((1) * (min(min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0), max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0), min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0))))), (0) + (max((1) * (min(min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0), max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0), min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0)))))), 0))))), ((0) + (min((1) * (min(min(min((0) + (min((1) * (min(min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0), max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0), min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0))))), (0) + (max((1) * (min(min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0), max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0), min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0)))))), 0), max(max((0) + (max((1) * (min(min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0), max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0), min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0))))), (0) + (min((1) * (min(min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0), max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0), min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0)))))), 0))), (1) * (max(max(max((0) + (max((1) * (min(min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0), max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0), min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0))))), (0) + (min((1) * (min(min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0), max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0), min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0)))))), 0), min(min((0) + (min((1) * (min(min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0), max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0), min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0))))), (0) + (max((1) * (min(min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0), max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0), min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0)))))), 0)))))) + (min((-1) * (min(min(min((0) + (min((1) * (min(min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0), max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0), min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0))))), (0) + (max((1) * (min(min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0), max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0), min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0)))))), 0), max(max((0) + (max((1) * (min(min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0), max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0), min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0))))), (0) + (min((1) * (min(min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0), max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0), min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0)))))), 0))), (-1) * (max(max(max((0) + (max((1) * (min(min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0), max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0), min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0))))), (0) + (min((1) * (min(min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0), max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0), min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0)))))), 0), min(min((0) + (min((1) * (min(min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0), max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0), min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0))))), (0) + (max((1) * (min(min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0), max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0), min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0)))))), 0)))))), 116);
if (z(15)) - (z(14)) <= 0
	d(36) = 1;
else
	d(36) = 0;
end

% secure_aux = secured >= 0.9;
within((0.9) - (z(16)), -0.1, 0.9, 118);
if (0.9) - (z(16)) <= 0
	d(37) = 1;
else
	d(37) = 0;
end

% phase = {IF ((passed_zone & isPH1) || (secure_aux & isPH2) || (on_drop & isPH3) || ((!closed) & on_drop & isPH4)) THEN (ph + 1) ELSE ph};
d(41) = ((((d(2)) & (d(25))) | ((d(37)) & (d(28)))) | ((d(4)) & (d(31)))) | (((~d(21)) & (d(4))) & (d(32)));

% closed_dist = {IF (cl_dst) THEN closed_dist_x ELSE closed_dist_y};
if d(36)
	within(z(14), min((0) + (min((1) * (min(min(min((0) + (min((1) * (min(min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0), max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0), min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0))))), (0) + (max((1) * (min(min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0), max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0), min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0)))))), 0), max(max((0) + (max((1) * (min(min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0), max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0), min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0))))), (0) + (min((1) * (min(min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0), max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0), min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0)))))), 0))), (1) * (max(max(max((0) + (max((1) * (min(min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0), max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0), min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0))))), (0) + (min((1) * (min(min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0), max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0), min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0)))))), 0), min(min((0) + (min((1) * (min(min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0), max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0), min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0))))), (0) + (max((1) * (min(min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0), max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0), min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0)))))), 0))))), (0) + (max((1) * (min(min(min((0) + (min((1) * (min(min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0), max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0), min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0))))), (0) + (max((1) * (min(min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0), max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0), min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0)))))), 0), max(max((0) + (max((1) * (min(min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0), max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0), min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0))))), (0) + (min((1) * (min(min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0), max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0), min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0)))))), 0))), (1) * (max(max(max((0) + (max((1) * (min(min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0), max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0), min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0))))), (0) + (min((1) * (min(min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0), max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0), min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0)))))), 0), min(min((0) + (min((1) * (min(min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0), max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0), min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0))))), (0) + (max((1) * (min(min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0), max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0), min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0)))))), 0)))))), max((0) + (max((1) * (min(min(min((0) + (min((1) * (min(min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0), max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0), min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0))))), (0) + (max((1) * (min(min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0), max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0), min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0)))))), 0), max(max((0) + (max((1) * (min(min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0), max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0), min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0))))), (0) + (min((1) * (min(min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0), max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0), min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0)))))), 0))), (1) * (max(max(max((0) + (max((1) * (min(min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0), max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0), min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0))))), (0) + (min((1) * (min(min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0), max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0), min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0)))))), 0), min(min((0) + (min((1) * (min(min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0), max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0), min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0))))), (0) + (max((1) * (min(min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0), max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0), min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0)))))), 0))))), (0) + (min((1) * (min(min(min((0) + (min((1) * (min(min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0), max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0), min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0))))), (0) + (max((1) * (min(min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0), max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0), min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0)))))), 0), max(max((0) + (max((1) * (min(min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0), max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0), min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0))))), (0) + (min((1) * (min(min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0), max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0), min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0)))))), 0))), (1) * (max(max(max((0) + (max((1) * (min(min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0), max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0), min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0))))), (0) + (min((1) * (min(min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0), max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0), min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0)))))), 0), min(min((0) + (min((1) * (min(min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0), max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0), min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0))))), (0) + (max((1) * (min(min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0), max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_x)) + (100), ((0) + (params.grab_pos_x)) + (-100)), 0), min(min(((0) + (params.grab_pos_x)) + (-100), ((0) + (params.grab_pos_x)) + (100)), 0)))))), 0)))))), 127);
	z(10) = z(14);
else
	within(z(15), min((0) + (min((1) * (min(min(min((0) + (min((1) * (min(min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0), max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0), min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0))))), (0) + (max((1) * (min(min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0), max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0), min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0)))))), 0), max(max((0) + (max((1) * (min(min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0), max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0), min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0))))), (0) + (min((1) * (min(min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0), max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0), min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0)))))), 0))), (1) * (max(max(max((0) + (max((1) * (min(min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0), max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0), min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0))))), (0) + (min((1) * (min(min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0), max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0), min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0)))))), 0), min(min((0) + (min((1) * (min(min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0), max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0), min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0))))), (0) + (max((1) * (min(min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0), max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0), min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0)))))), 0))))), (0) + (max((1) * (min(min(min((0) + (min((1) * (min(min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0), max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0), min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0))))), (0) + (max((1) * (min(min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0), max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0), min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0)))))), 0), max(max((0) + (max((1) * (min(min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0), max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0), min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0))))), (0) + (min((1) * (min(min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0), max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0), min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0)))))), 0))), (1) * (max(max(max((0) + (max((1) * (min(min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0), max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0), min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0))))), (0) + (min((1) * (min(min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0), max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0), min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0)))))), 0), min(min((0) + (min((1) * (min(min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0), max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0), min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0))))), (0) + (max((1) * (min(min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0), max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0), min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0)))))), 0)))))), max((0) + (max((1) * (min(min(min((0) + (min((1) * (min(min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0), max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0), min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0))))), (0) + (max((1) * (min(min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0), max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0), min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0)))))), 0), max(max((0) + (max((1) * (min(min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0), max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0), min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0))))), (0) + (min((1) * (min(min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0), max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0), min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0)))))), 0))), (1) * (max(max(max((0) + (max((1) * (min(min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0), max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0), min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0))))), (0) + (min((1) * (min(min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0), max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0), min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0)))))), 0), min(min((0) + (min((1) * (min(min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0), max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0), min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0))))), (0) + (max((1) * (min(min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0), max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0), min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0)))))), 0))))), (0) + (min((1) * (min(min(min((0) + (min((1) * (min(min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0), max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0), min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0))))), (0) + (max((1) * (min(min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0), max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0), min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0)))))), 0), max(max((0) + (max((1) * (min(min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0), max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0), min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0))))), (0) + (min((1) * (min(min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0), max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0), min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0)))))), 0))), (1) * (max(max(max((0) + (max((1) * (min(min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0), max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0), min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0))))), (0) + (min((1) * (min(min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0), max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0), min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0)))))), 0), min(min((0) + (min((1) * (min(min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0), max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0), min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0))))), (0) + (max((1) * (min(min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0), max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0))), (1) * (max(max(max(((0) + (params.grab_pos_y)) + (100), ((0) + (params.grab_pos_y)) + (-100)), 0), min(min(((0) + (params.grab_pos_y)) + (-100), ((0) + (params.grab_pos_y)) + (100)), 0)))))), 0)))))), 127);
	z(10) = z(15);
end

% phase = {IF ((passed_zone & isPH1) || (secure_aux & isPH2) || (on_drop & isPH3) || ((!closed) & on_drop & isPH4)) THEN (ph + 1) ELSE ph};
if d(41)
	within((x(11)) + (1), 2, 11, 138);
	z(9) = (x(11)) + (1);
else
	within(x(11), 1, 10, 138);
	z(9) = x(11);
end

% bg = block_gripper;
y(5) = z(3);

% closed_out = closed_dist;
y(3) = z(10);

% dist = dist_calc_x;
y(1) = z(12);

% fclosed = closed_aux;
xn(12) = d(35);

% inter = !closed_aux & (!prepared) & isPH2;
y(6) = d(42);

% pen = (REAL pen_aux);
y(2) = d(24);

% ph = phase;
xn(11) = z(9);

% theta = theta + Ts * theta_dot;
xn(1) = (x(1)) + ((params.Ts) * (x(2)));

% theta_dot = dtheta;
xn(2) = z(4);

% x = x + Ts * x_dot;
xn(3) = (x(3)) + ((params.Ts) * (x(4)));

% x_dot = dvx;
xn(4) = z(5);

% y = y + Ts * y_dot;
xn(5) = (x(5)) + ((params.Ts) * (x(6)));

% y_dot = dvy;
xn(6) = z(6);

% yaw = yaw + Ts * yaw_dot;
xn(9) = (x(9)) + ((params.Ts) * (x(10)));

% yaw_dot = dyaw;
xn(10) = z(8);

% z = z + Ts * z_dot;
xn(7) = (x(7)) + ((params.Ts) * (x(8)));

% z_dot = dvz;
xn(8) = z(7);

% zone = zone_u;
y(4) = z(11);

xn=xn(:);
y=y(:);
z=z(:);
d=d(:);


function within(x, lo, hi, line)
 if x<lo | x>hi 
 error(['bounds violated at line ', num2str(line), ' in the hysdel source']); 
 end
