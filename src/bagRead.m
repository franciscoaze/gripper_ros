clear all
clc
addpath('/home/dsor-replace2/.ros')  
bag = rosbag('ardu.bag');
n_topics = size(bag.AvailableTopics,1);
data = {};
figure('Name','Arduino bags')

for t=1:n_topics
   topic = bag.AvailableTopics.Row(t);
   t_data = select(bag,'Topic',topic);
   data{t} = timeseries(t_data);
   data{t}.setprop('Name',topic);
   subplot(n_topics,1,t)
   plot(data{t}.Time-bag.StartTime,data{t}.Data)
   title(topic)
end