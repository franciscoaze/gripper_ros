#!/usr/bin/env python

import rospy
from pytests.msg import PPose
import cv2
import cv2.aruco as aruco
import numpy as np
import createBoxAruco as cc
import math
import time
import sys

####################################################
############## Auxiliary Calculations ##############
####################################################
def eulerRY(theta):
    """
    Calculates Rotation Matrix given euler angles.
    """

    thetaRad = math.radians(theta)

    R_y = np.array([[math.cos(thetaRad), 0, math.sin(thetaRad)],
                    [0, 1, 0],
                    [-math.sin(thetaRad), 0, math.cos(thetaRad)]
                    ])

    return R_y

def rotationMatrixToEulerAngles(R):
    """
    Calculates Euler Angles given a Rotation Matrix.
        The result is the same as MATLAB except the order
        of the euler angles ( x and z are swapped ).
        """

    assert (isRotationMatrix(R))

    sy = math.sqrt(R[0, 0] * R[0, 0] + R[1, 0] * R[1, 0])

    singular = sy < 1e-6

    if not singular:
        x = math.atan2(R[2, 1], R[2, 2])
        y = math.atan2(-R[2, 0], sy)
        z = math.atan2(R[1, 0], R[0, 0])
    else:
        x = math.atan2(-R[1, 2], R[1, 1])
        y = math.atan2(-R[2, 0], sy)
        z = 0

    return np.array([x, y, z])

def calcRotation(rvecs, tvecs):
    """
    Calculates Euler Angles given rotation and position vectors.
        The result is the same as MATLAB except the order
        of the euler angles ( x and z are swapped ).
        """
    R, jac = cv2.Rodrigues(rvecs)
    Wpos = np.matmul(-R.transpose(), tvecs)

    sy = math.sqrt(R[0, 0] * R[0, 0] + R[1, 0] * R[1, 0])
    singular = sy < 1e-6

    if not singular:
        x = math.degrees(math.atan2(R[2, 1], R[2, 2]))
        y = math.degrees(math.atan2(-R[2, 0], sy))
        z = math.degrees(math.atan2(R[1, 0], R[0, 0]))
    else:
        x = math.degrees(math.atan2(R[1, 2], R[1, 1]))
        y = math.degrees(math.atan2(-R[2, 0], sy))
        z = 0

    return np.array([x, y, z]), Wpos

def isRotationMatrix(R):
    """
    Checks if a matrix is a valid rotation matrix.
    """
    Rt = np.transpose(R)
    shouldBeIdentity = np.dot(Rt, R)
    I = np.identity(3, dtype=R.dtype)
    n = np.linalg.norm(I - shouldBeIdentity)
    return n < 1e-6


def transformPose(rot_vector, pos_vector):
    """
    Transforms ArUco rotation and position vectors into the
    desired coordinate system.
    Transformations:
        * Position of the Camera in Parcel body frame
            P_pos_C = p2cR * C_pos_P
        * Rotation from Camera to Drone body frame
            c2bR = rotY(30)
        * Position of Camera in Drone body frame
            C_pos_B = -lo
        * Position of Parcel in Drone body frame
            B_pos_P = C_pos_B + c2bR * C_pos_P
        * Rotation from Parcel to Drone body frame
            p2bR = c2bR * p2cR

    :param rot_vector: object rotation in respect to the camera. Corresponds to p2cR (using Rodrigues Formula)
    :param pos_vector: object position in respect to the camera. Corresponds to C_pos_P
    :return: P_pos_C, angles
    """
    p2cR, _ = cv2.Rodrigues(rot_vector)

    c2pR = -p2cR.transpose()

    p_pos_c = c2pR.dot(pos_vector)

    # lo = np.array([-0.1, 0, -0.1])
    # c2bR = eulerRY(30)
    # P_pos_W = lo + c2bR.dot(P_pos_c)

    angles = rotationMatrixToEulerAngles(p2cR)

    return p_pos_c, angles

####################################################
################# Class Definition #################
####################################################
class ArucoPose():
    """
    ArucoPose() class
    Defines the necessary variables and properties to initialize
        the ArUco module adapted to our problem. Runs a loop
        publishing the acquired object pose to a predefined topic.
    """
    def __init__(self):
        # ARUCO AND CAMERA INITIALIZATION
        # Get video input from 0.
        self.cap = cv2.VideoCapture(0)

        # If video not available, exit node stop running this script.
        if not self.cap.isOpened():
            print('[ArUco]: Camera is not connected.')
            rospy.signal_shutdown('Camera not connected.')
            sys.exit()

        # Define basic video parameters
        self.cap.set(cv2.CAP_PROP_AUTOFOCUS, 1)
        self.cap.set(cv2.CAP_PROP_FRAME_WIDTH, 800)
        self.cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 600)

        # Set live viewing window
        cv2.namedWindow('frame', cv2.WINDOW_NORMAL)
        cv2.resizeWindow('frame', 800 * 2 / 3, 600 * 2 / 3)

        # Set video output settings. If save_video== True, a file named output.avi will be created in this directory.
        self.save_video = False
        if self.save_video:
            self.fourcc = cv2.VideoWriter_fourcc(*'XVID')
            self.out = cv2.VideoWriter('output.avi', self.fourcc, 30.0, (800, 600))

        # Get ArUco object information from createBoxAruco.py script and define its properties.
        self.box = cc.markerBox(face_side=0.077, nr_markers=9, marker_separation=0.0025,
                                 boxL=0.162, boxH=0.131, boxW=0.121)
        self.aruco_board = self.box.aruco_board
        self.aruco_dict = self.aruco_board.dictionary

        # Get the calibration matrices from saved files.
        # distortion coefficient vector:
        self.dist = np.genfromtxt(
            "CameraMtx/calib_dist_webcam.csv")
        # camera's intrinsic matrix:
        self.mtx = np.genfromtxt(
            "CameraMtx/calib_mtx_webcam.csv")

        # Initialize main variables
        self.rvecs = None
        self.tvecs = None
        self.euler = None
        self.Wpos = None

        # Create parameters object
        self.parameters = aruco.DetectorParameters_create()

        # START ROS NODE
        rospy.init_node('aruco_node', anonymous=True)
        self.pub = rospy.Publisher('aruco_pose', PPose, queue_size=10)
        self.send_msg = PPose()
        self.rate = rospy.Rate(24)  # 24hz , go through the loop 24 times per second

    def getPose(self):
        error_flag = True
        start_time = time.time()
        fps_time = start_time
        fps = 0
        n_frames = 0
        ids_used = 0

        # Main ArUco loop. Runs while is ROS is online.
        while not rospy.is_shutdown():
            # Capture frame-by-frame
            ret, frame = self.cap.read()
            # Convert frame to grayscale. This line could be omitted and the original RGB image could be used.
            # However, using an RGB colorspace to perform point analysis does not substantially improve results
            # and will greatly increase the computational cost of the operation.
            gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

            self.send_msg.ids = []
            # Detect markers in the image using ArUco's detectMarkers() function.
            corners, ids, rejectedImgPoints = aruco.detectMarkers(gray, self.aruco_dict, parameters=self.parameters)
            aruco.refineDetectedMarkers(gray, self.aruco_board, corners, ids, rejectedImgPoints, self.mtx, self.dist)

            frame = aruco.drawDetectedMarkers(frame, corners)

            n_frames += 1
            # Calculate real frame rate
            if n_frames >= 30:
                t = time.time() - fps_time
                fps = round(n_frames / t, 2)
                n_frames = 0
                fps_time = time.time()
                self.send_msg.fps = fps

            try:
                if len(corners) > 0: # If there are any detected markers in the image
                    # Estimate the object pose using ArUco's estimatePoseBoard() function.
                    ids_used, self.rvecs, self.tvecs = aruco.estimatePoseBoard(corners, ids, self.aruco_board, self.mtx,
                                                                               self.dist, self.rvecs, self.tvecs)
                    # Draw the estimated coordinate axis in the image
                    frame = aruco.drawAxis(frame, self.mtx, self.dist, self.rvecs, self.tvecs, 0.08)
                    self.euler, self.Wpos = calcRotation(self.rvecs, self.tvecs)
                    self.send_msg.ids = ids
                    self.send_msg.status = -1

                if self.tvecs is not None: # If the pose was well acquired.
                    P_pos_c, angles = transformPose(self.rvecs, self.tvecs)
                    self.send_msg.rot_vector = self.rvecs
                    self.send_msg.pos_vector = self.tvecs
                    self.send_msg.PposC = P_pos_c
                    self.send_msg.euler = angles
                    self.send_msg.status = 1
                self.send_msg.time = time.time() - start_time
                error_flag = False
            except: # The detected markers on the image are not related to the predefined object points
                if error_flag == False:
                    print("[ArUco]: Board is not correctly defined")
                error_flag = True
                self.rvecs = None
                self.tvecs = None
                self.euler = None
                self.Wpos = None
                # print("Stop B")

            self.send_msg.header.stamp = rospy.Time.now()
            self.pub.publish(self.send_msg)

            # Draw and show image to current window
            frame = self.drawCrosshair(frame)
            cv2.imshow('frame', frame)

            if self.save_video:
                self.out.write(frame)

            # Press 'q' to exit the loop
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break

            self.rate.sleep()

        self.cap.release()
        cv2.destroyAllWindows()
        if self.save_video:
            self.out.release()
        for i in range(7):
            cv2.waitKey(1)

    def drawCrosshair(self, frame):
        # Draw crossair
        cv2.line(frame, (2 * frame.shape[1] // 5, frame.shape[0] // 2), (3 * frame.shape[1] // 5, frame.shape[0] // 2),
                 (0, 255, 255), 2)
        cv2.line(frame, (frame.shape[1] // 2, 2 * frame.shape[0] // 5), (frame.shape[1] // 2, 3 * frame.shape[0] // 5),
                 (0, 255, 255), 2)
        # Vertical line with width=1 is not showing...
        return frame


if __name__ == '__main__':
    try:
        AR = ArucoPose()
        AR.getPose()

    except rospy.ROSInterruptException:
        pass
