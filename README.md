# Gripper ROS
ROS package for the gripper mechanism implemantation in a drone system.

Use catkin build to to compile the ROS package. 

All relevant scripts can be found under the **src** folder. The arduino scripts can be found under the **Arduino/Gripper_ROS** folder.

A more complete guide to this compilation can be found [here](https://docs.google.com/document/d/1FYDBhZuZDhOA5J17wX9bda1tC2WLqTvOo4odXEji-Uw/edit?usp=sharing
).

### Main components
These launch files combine most of the operations developed in this package.

+ **gripper_nodes.launch**: perform basic tests on the gripper movement and camera.

+ **cplex_nodes.launch**: main simulation combining all the different modules (drone, MPC, camera, arduino).