void blinkX(int ntimes) {
  for (int n = 1; n <= ntimes; n++) {
    PORTB = B000011;
    delay(200);
    PORTB = B000000;
    delay(150);
  }
}

int decodeServoVal(float val){
  if(val > 0.05){
    servoVal = ccwZone[0] + int((ccwZone[1]-ccwZone[0]) * val);
  } else if(val < -0.05){
    servoVal = cwZone[1] + int((cwZone[1]-cwZone[0]) * val);
  } else{
    servoVal = int((ccwZone[0]+cwZone[1])/2);
  }

  return servoVal;
}

void homePosition(){
  digitalWrite(switchPin,HIGH);

  pos = analogRead(pospot);  
  int pos_map = 90 - map(pos, 0, 410, 0, 90);
  delay(10);
//  Serial.println("Gripper arm is at " + String(pos_map) + " degrees.");
//  Serial.println("Home position is at " + String(homepos) + " degrees.");
//  Serial.println("Arm will move " + String(homepos-pos_map) + " degrees.");
  float error = homepos - pos_map;
  while(abs(error)>0.1){
      pos = 90 - map(analogRead(pospot), 0, 410, 0, 90);
      error = homepos - pos;
      val = error*0.05;
      servoVal = decodeServoVal(val);
      myservo.writeMicroseconds(servoVal);
      delay(10);
//      Serial.println(String(error) + " | " + String(val));
  }
  myservo.write(servoSTOP);
  digitalWrite(switchPin,LOW);
  digitalWrite(LEDG,HIGH);
  Serial.println("OK");
  delay(100);
}

void safetyStop(){
  myservo.write(servoSTOP);
  blinkX(5);
  if (intString > (scale * offset)){
    servoVal = -0.5;
  } else{
    servoVal = 0.5;
  }
  while (true){
    int current = analogRead(A0);
    myservo.write(servoVal);
    if (current < currThresh){
      break;
    }
  }
  intString = scale * offset;
}

void checkSerial() {
  if (estabilished == true) {
    while (Serial.available()) {
      // get the new byte:
      char inChar = (char)Serial.read();
      // add it to the inputString:
      // if the incoming character is a newline, set a flag so the main loop can
      // do something about it:
      if (inChar == '\n') {
        if (inputString.startsWith("A")) {
          stringComplete = true;
          intString = inputString.substring(1).toInt();
          inputString = "";
          last_tm = millis();
//          countStall=0;
        } else {
          intString = intString;
          inputString = "";
          stringComplete = false;         
        }
      } else {
        inputString += inChar;
      }
    }
  }
}
