// NECESSARY LIBRARIES --------------------
#include <Servo.h>

Servo myservo;

// MOTOR PARAMETERS------------------------
// servoSTOP: reference angle for which the motor stops
// cwZone: interval of pulse durations (in uS) where the motor is rotating in clockwise motion
// ccwZone:interval of pulse durations (in uS) where the motor is rotating in counter-clockwise motion

//#define servoSTOP 84 // MG995
//#define servoSTOP 72 // Futaba
#define servoSTOP 91 //TSD99X
// TSD99-X     *
//int cwZone[] = {1180,1480}; //in uS
//int ccwZone[] = {1495,1780}; //in uS
//int cwZone[] = {63,91}; //in deg
//int ccwZone[] = {93,127}; //in deg
// TSD99-X MOD *
int cwZone[] = {1240,1431}; //in uS
int ccwZone[] = {1435,1635}; //in uS

// PIN DEFINITIONS-------------------------
#define LEDR 8
#define LEDG 9
#define LEDB 7
#define buttonCal 4
#define buttonStop 3
#define switchPin 2
#define pospot A4
#define calpot A3

// VARIABLE DEFINITIONS ------------------
bool estabilished = false; // ensure the serial port is only checked after setup
String inputString = "";  // a String to hold incoming data
bool stringComplete = false;  // whether the string is complete
int pos; // gripper angle
float val;  // decoded motor input [-1,1]
int servoVal = cwZone[1]; // decoded motor intpu in uS
float offset = 10; // offset constant
float scale = 100; // scalling constant
int intString = offset * scale; // value received from serial
int delayServo = 10; // delay after sending input to servo
int originalThresh = 200; // original current threshold value
int currThresh = originalThresh; // modified current threshold value
int countStall = 0; // count of stall moments
int stall = 0; // if motor is stalled
int curr_stall =0; // if motor is stalled (when circuit close)
unsigned long last_tm; // last time a value was received via serial
int homepos = 30; // default homing  position
String homeString = ""; // string received for homing


void setup() {
  // Set the pins
  pinMode(LEDR, OUTPUT);
  pinMode(LEDG, OUTPUT);
  pinMode(LEDB, OUTPUT);
  pinMode(13, OUTPUT);
  pinMode(buttonCal, INPUT_PULLUP);
  pinMode(buttonStop, INPUT_PULLUP);
  pinMode(switchPin, OUTPUT);

  // Turn off LEDS
  digitalWrite(LEDG, LOW);
  digitalWrite(LEDR, LOW);
  digitalWrite(LEDB, LOW);

  // Set servos
  myservo.attach(5);
  myservo.write(servoSTOP);

  Serial.begin(115200);

  // Ensure that the first handshake message is received from a external script.
  // Order of operations: 1-wait for serial message; 2-read message with "O" or "K"; 3-perform homing; 4-send "OK"
  bool bl = true;
  while (bl) {
    if (digitalRead(buttonStop) == LOW){
      blinkX(2); 
      bl = false;
    }
    if (Serial.available()) {
      char inChar = (char)Serial.read();
      if (inChar == '\n') {
        if (homeString.startsWith("O")) {
          // If string starts with "O", proceed to home the gripper. Digits after the "O" correspond to the desired angle.
          if (homeString.length()>1){
            homepos = homeString.substring(1).toInt();
          }
          blinkX(2); 
          homePosition();
          bl = false;
        }else if(homeString.startsWith("K")){
          // If string starts with "K", gripper will not be homed.
          blinkX(3);
          bl = false;
        }
        homeString = "";
      }else{
        homeString += inChar;
          }
    }  
  }
  estabilished = true;
  last_tm = millis();

  digitalWrite(switchPin, HIGH); 
}

void loop() {
  if (digitalRead(buttonStop) == LOW){
    // Emergency home operation
    homePosition();
  }

  // Check if any message has arrived from the serial connection
  checkSerial();

  // Set current threshold
  int Thresh_pot = map(analogRead(calpot),0,1024,-50,50);
  currThresh = originalThresh + Thresh_pot;
  delay(10);

  // Read current and compare it to threshold
  int current = analogRead(A0);
  if (current >= currThresh){
    countStall +=1;
    if (countStall > 3){
      // if current is higher than threshold more than X times, then motor is stalled
      stall = 1;
    }
  } else{
    countStall = 0;
    stall = 0;
  }

  // Turn blue LED on when motor is stalled
  if ((stall)&(intString != (scale * offset))){
    digitalWrite(LEDB,HIGH);
  }else{
    digitalWrite(LEDB,LOW);
  }
  
  // Read and convert potentiometer value to gripper angle
  pos = analogRead(pospot);
  int pos_map = 90 - map(pos, 0, 400, 0, 90);

  // Send message with sensors data to serial port
  Serial.println(String(currThresh)+","+String(pos_map)+ "," + String(current)+","+String(stall));
  
  // Decode the motor input into [-1,1]; 
  val = (intString / scale) - offset; 

  if ((millis()-last_tm)>1000){
    // If the arduino hasnt received a message from the serial port for more than X seconds, then stop the motor
    servoVal = decodeServoVal(0); // Decode the motor input into uS;
    myservo.writeMicroseconds(servoVal);
    digitalWrite(switchPin, LOW);
    delay(delayServo);
    PORTB = B000000; //
    
  } else {
    if (val > 0) {
      // Gripper is grasping
      servoVal = decodeServoVal(val);// Decode the motor input into uS;
      digitalWrite(switchPin, HIGH);
      myservo.writeMicroseconds(servoVal);  
  //    myservo.write(servoVal);
      delay(delayServo);
      PORTB = B000010; // Green ON, Red OFF
      
    } else if (val < 0) {
      // Gripper is opening
      servoVal = decodeServoVal(val);// Decode the motor input into uS;
      digitalWrite(switchPin, HIGH);
      myservo.writeMicroseconds(servoVal);
  //    myservo.write(servoVal);
      delay(delayServo);
      PORTB = B000001; // Red ON, Green OFF
      
    } else if (intString == (scale * offset)) {
      // Gripper is stoped
      servoVal = decodeServoVal(val); // Decode the motor input into uS;
      myservo.writeMicroseconds(servoVal);
      digitalWrite(switchPin, LOW);  
      delay(delayServo);
      PORTB = B000000; // LEDS off
    }
  }
}
